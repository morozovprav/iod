<?php

namespace App\Traits;

use App\Models\SeoText;

trait SeoTexts
{
    public function seoTexts()
    {
        return $this->morphMany(SeoText::class, 'seoable');
    }

    public static $management = [
        "Understanding and using in speech familiar phrases and expressions necessary to perform specific tasks. Ability to introduce himself or introduce others, ask or answer simple questions. The ability to participate in a simple conversation, if the interlocutor speaks slowly and clearly.
"
    ];
}
