<?php

namespace App\Models;
use App\Traits\TranslateAndConvertMediaUrl;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spatie\Translatable\HasTranslations;

class About extends Model
{
    use HasFactory, HasTranslations,TranslateAndConvertMediaUrl;

    protected $translatable = [
        'hero_h1_title',
        'hero_img',
        'hero_description_left',
        'hero_description_botom',
        'advantage',
        'numeric_list',
        'mark_list',
        'paragraphs',
        'galery',
        'advantage_botom',
        'meta_title',
        'meta_description',
        'meta_keyword',
        'og_title',
        'og_description',
        'og_img',
        'mark_list_title',
        'numeric_list_title'
    ];
    protected $fillable = [
        'hero_h1_title',
        'hero_img',
        'hero_description_left',
        'hero_description_botom',
        'advantage',
        'numeric_list',
        'mark_list',
        'paragraphs',
        'galery',
        'advantage_botom',
        'meta_title',
        'meta_description',
        'meta_keyword',
        'og_title',
        'og_description',
        'og_img',
        'mark_list_title',
        'numeric_list_title'
    ];
    public $mediaToUrl = [
        'galery',
        'img',
        'hero_img',
        'og_img',
        'advantage',
        'video',
    ];

    public static function normalizeData($object){
        self::getNormalizedField($object, 'galery', 'img', true, true);
        self::getNormalizedField($object, 'hero_description_botom','key',true,true);
        self::getNormalizedField($object, 'advantage','key',true,true);
        self::getNormalizedField($object, 'numeric_list','key',true,true);
        self::getNormalizedField($object, 'mark_list','key',true,true);
        self::getNormalizedField($object, 'paragraphs','key',true,true);
        self::getNormalizedField($object, 'advantage_botom','key',true,true);

        foreach ($object['numeric_list'] as $item){
            $numericList[] = $item['item_description'];
        }
        $object['numeric_list'] = [
            'title' => $object['numeric_list_title'],
            'list' => $numericList
        ];
        unset($object['numeric_list_title']);

        foreach ($object['mark_list'] as $item){
            $markList[] = $item['item_description'];
        }
        $object['mark_list'] = [
            'title' => $object['mark_list_title'],
            'list' => $markList
        ];
        unset($object['mark_list_title']);
        return $object;
    }

    public function  getFullData(){

        try{
            $data = $this->getAllWithMediaUrlWithout(['id','created_at','updated_at']);
            $data = self::normalizeData($data);
            return $data;
        }catch (\Exception $ex){
            throw new ModelNotFoundException();
        }

    }
}
