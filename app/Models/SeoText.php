<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeoText extends Model
{
    use HasFactory;

    protected $fillable = [
        'content',
        'seoable_id',
        'seoable_type',
    ];

    public function seoable()
    {
        return $this->morphTo();
    }
}
