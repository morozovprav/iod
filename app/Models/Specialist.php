<?php

namespace App\Models;

use App\Filters\QueryFilter;
use App\Traits\TranslateAndConvertMediaUrl;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spatie\Translatable\HasTranslations;

/**
 * @method static whereIn(string $string, array $specialistId)
 */
class Specialist extends Model
{
    use HasFactory, HasTranslations, TranslateAndConvertMediaUrl;

    protected $fillable = [
        'meta_title',
        'meta_description',
        'meta_keyword',
        'og_title',
        'og_description',
        'og_img',
        'name',
        'main_img',
        'slug',
        'about_me',
        'projects',
        'certificates',
        'courses',
        'english_level_id',
        'profession_id',
        'status_id',
        'english_is_checked',
        'display',
        'portfolio_link',
    ];

    public $translatable = [
        'meta_title',
        'meta_description',
        'meta_keyword',
        'og_title',
        'og_description',
        'name',
        'about_me',
        'projects',
        'certificates',
        'courses',
        'exp_info'
    ];

    public $mediaToUrl = [
        'main_img',
        'og_img',
        'img',
        'certificates',
    ];

    /**
     * Scope a query to only include popular users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDisplay($query)
    {
        return $query->where('display', 1);
    }

    public function scopeGetSpecializationWithExperience($query){
        return $query->with('specializations', function ($q){
            $q->select('specializations.id',
                'specializations.title',
                'specializations.profession_id',
                'experiences.id as exp_id',
                'experiences.title as exp_title',
                'experiences.info as exp_info',
                'experiences.sort_order as exp_sort_order',
            );
            $q->join('experiences', 'experiences.id', '=', 'specialist_specialization.experience_id');
            if(request()->has('specialization')){
                $q->whereIn('specializations.id', explode(',', request('specialization')));
            }
            $q->orderBy('exp_sort_order');
        });
    }

    public function scopeFilter(Builder $builder, QueryFilter $filters)
    {
        return $filters->apply($builder);
    }

    public function teams(){
        return $this->belongsToMany(Team::class, 'specialist_team', 'specialist_id', 'team_id');
    }

    public static function normalizeData($object){
        self::getNormalizedField($object, 'projects', 'title', true, true);
        self::getNormalizedField($object, 'certificates', 'link', true, true);
        self::getNormalizedField($object, 'courses', 'title', true, true);

        if (array_key_exists('status', $object) and !empty($object['status'])){
            $object['status'] = $object['status'][0]['title'];
        }
//        $object['englishLevel'] = $object['englishLevel'][0]['title'];
//        $object['profession'] = $object['profession'][0]['title'];

        return $object;
    }

    public function getFullData(){
        try{
            $data = $this->getAllWithMediaUrlWithout(['created_at','updated_at']);
            return self::normalizeData($data);
        }catch (\Exception $ex){
            throw new ModelNotFoundException();
        }
    }

    public function englishLevel() {
        return $this->belongsTo(EnglishLevel::class);
    }

    public function profession() {
        return $this->belongsTo(Profession::class);
    }

    public function main()
    {
        return $this->belongsToMany(Main::class, 'main_specialists');
    }

    public function status() {
        return $this->belongsTo(Status::class);
    }

    public function specializations()
    {
        return $this->belongsToMany(Specialization::class, 'specialist_specialization')
//            ->orderBy('sort_order')
            ->using('App\Models\SpecialistSpecialization')
            ->withPivot('experience_id');
    }

    public function technologies() {
        return $this->belongsToMany(Technology::class, 'specialist_technology');
    }
}
