<?php

namespace App\Models;

use App\Traits\TranslateAndConvertMediaUrl;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spatie\Translatable\HasTranslations;

class SpecialistsPage extends Model
{
    use HasFactory, HasTranslations,TranslateAndConvertMediaUrl;

    protected $translatable = [
        'meta_title',
        'meta_description',
        'meta_keyword',
        'og_title',
        'og_description',
        'h1_title',
        'search_field_placeholder',
        'only_free_candidate_label',
        'reset_filter_label',
    ];
    protected $fillable = [
        'meta_title',
        'meta_description',
        'meta_keyword',
        'og_title',
        'og_description',
        'og_img',
        'h1_title',
        'search_field_placeholder',
        'only_free_candidate_label',
        'reset_filter_label',
        'technology_amount',
    ];
    public $mediaToUrl = [
        'og_img',
    ];

    public static function normalizeData($object){

        return $object;
    }

    public function  getFullData(){

        try{
            $data = $this->getAllWithMediaUrlWithout(['id','created_at','updated_at']);
            $data = self::normalizeData($data);
            return $data;
        }catch (\Exception $ex){
            throw new ModelNotFoundException();
        }

    }
}
