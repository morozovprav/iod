<?php

namespace App\Models;

use App\Traits\SeoTexts;
use App\Traits\TranslateAndConvertMediaUrl;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spatie\Translatable\HasTranslations;

class EnglishLevel extends Model
{
    use HasFactory, HasTranslations, TranslateAndConvertMediaUrl, SeoTexts;

    protected $fillable = [
        'title',
        'info',
    ];

    public $translatable = [
        'info',
    ];

    public function specialists() {
        return $this->hasMany(Specialist::class);
    }

    public static function normalizeData($object){
        return $object;
    }

    public function fieldsForGenerator(){
        return $this->select('id', 'title as name');
    }

    public function getFullData(){
        try{
            $data = $this->getAllWithMediaUrlWithout(['created_at','updated_at']);
            return $data;
        }catch (\Exception $ex){
            throw new ModelNotFoundException();
        }
    }
}
