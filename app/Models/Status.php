<?php

namespace App\Models;

use App\Traits\TranslateAndConvertMediaUrl;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spatie\Translatable\HasTranslations;

class Status extends Model
{
    use HasFactory, HasTranslations, TranslateAndConvertMediaUrl;

    protected $fillable = [
        'title',
    ];

    public $translatable = [
        'title',
    ];

    public function specialists() {
        return $this->hasMany(Specialist::class);
    }

    public function fieldsForGenerator(){
        return $this->select('id', 'title->en as name');
    }

    public static function normalizeData($object){
        return $object;
    }

    public function getFullData(){
        try{
            $data = $this->getAllWithMediaUrlWithout(['created_at','updated_at']);
            return $data;
        }catch (\Exception $ex){
            throw new ModelNotFoundException();
        }
    }
}
