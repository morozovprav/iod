<?php

namespace App\Models;

use App\Traits\SeoTexts;
use App\Traits\TranslateAndConvertMediaUrl;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Nova\Fields\BelongsToMany;
use Spatie\Translatable\HasTranslations;

class Specialization extends Model
{
    use HasFactory, HasTranslations, TranslateAndConvertMediaUrl, SeoTexts;

    protected $fillable = [
        'title',
        'profession_id',
    ];

    public $translatable = [
        'title',
        'exp_info'
    ];

    public function specialists() {
        return $this->BelongsToMany(Specialist::class, 'specialist_specialization')
//            ->using(SpecialistSpecialization::class)
            ->withPivotValue('experience_id');
    }
//TODO при запросе через whereHas если есть withPivotValue возникает ошибка. Пока не понял как ее изменить.
    public function specialistsForFilters() {
        return $this->BelongsToMany(Specialist::class, 'specialist_specialization');
    }

    public function profession(){
        return $this->belongsTo(Profession::class, 'profession_id');
    }

    public function technologies(){
        return $this->belongsToMany(Technology::class, 'specialization_technology');
    }

    public function fieldsForGenerator(){
        return $this->select('id', 'title->en as name', 'profession_id');
    }

    public static function normalizeData($object){
        return $object;
    }

    public function getFullData(){
        try{
            $data = $this->getAllWithMediaUrlWithout(['created_at','updated_at']);
            return $data;
        }catch (\Exception $ex){
            throw new ModelNotFoundException();
        }
    }
}
