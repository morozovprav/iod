<?php

namespace App\Models;


use App\Traits\TranslateAndConvertMediaUrl;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spatie\Translatable\HasTranslations;

class Contact extends Model
{
    use HasFactory,HasTranslations,TranslateAndConvertMediaUrl;

    public $translatable = [
        'h1_title',
        'work_hours_label',
        'work_hours',
        'work_hours_time_zone_city',
        'office_address_label',
        'office_address',
        'email_label',
        'email',
        'phone_label',
        'phone',
        'social_label',
        'social',
        'form_name_placeholder',
        'form_email_placeholder',
        'form_phone_placeholder',
        'form_textarea_placeholder',
        'form_privacy_first_part_label',
        'form_privacy_second_part_label',
        'form_button_title',
        'meta_title',
        'meta_description',
        'meta_keyword',
        'og_title',
        'og_description',
        'og_img',
        'messengers_label',
        'messengers',
        'form_file_label',
        'thank_you_title',
        'thank_you_description',
        'thank_you_btn_title',

    ];
    protected $fillable = [
        'h1_title',
        'work_hours_label',
        'work_hours',
        'work_hours_time_zone_city',
        'office_address_label',
        'office_address',
        'email_label',
        'email',
        'phone_label',
        'phone',
        'social_label',
        'social',
        'form_name_placeholder',
        'form_email_placeholder',
        'form_phone_placeholder',
        'form_textarea_placeholder',
        'form_privacy_first_part_label',
        'form_privacy_second_part_label',
        'form_button_title',
        'meta_title',
        'meta_description',
        'meta_keyword',
        'og_title',
        'og_description',
        'og_img',
        'messengers_label',
        'messengers',
        'form_file_label',
        'thank_you_title',
        'thank_you_description',
        'thank_you_btn_title',
    ];

    public static function normalizeData($object){
            self::getNormalizedField($object, 'messengers', 'messenger_link', true, true);
            if (array_key_exists('messengers', $object)){
                foreach ($object['messengers'] as $messenger){
                    $messengers[$messenger['messenger_title']] = $messenger['messenger_link'];
                }
                $object['messengers'] = $messengers;
            }

        return $object;
    }

    public function  getFullData(){

        try{
            $data = $this->getAllWithMediaUrlWithout([
                'id',
                'created_at',
                'updated_at',
                ]);
            return self::normalizeData($data);
        }catch (\Exception $ex){
            throw new ModelNotFoundException();
        }
    }

    public function  getFullDataForContact(){

        try{
            $data = $this->getAllWithMediaUrlWithout([
                'id',
                'created_at',
                'updated_at',
                'form_name_placeholder',
                'form_email_placeholder',
                'form_phone_placeholder',
                'form_textarea_placeholder',
                'form_privacy_first_part_label',
                'form_privacy_second_part_label',
                'form_file_label',
                'form_button_title',
                'email',
                'phone',
                'messengers',
                'messengers_label',
                'thank_you_title',
                'thank_you_description',
                'thank_you_btn_title',]);
            return self::normalizeData($data);
        }catch (\Exception $ex){
            throw new ModelNotFoundException();
        }
    }
}
