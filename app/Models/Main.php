<?php

namespace App\Models;

use App\Traits\TranslateAndConvertMediaUrl;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spatie\Translatable\HasTranslations;


class Main extends Model
{
    use HasFactory, HasTranslations,TranslateAndConvertMediaUrl;

    protected $translatable = [
        'title',
        'description',
        'h1_text',
        'h2_text',
        'see_all_btn_txt',

        'meta_title',
        'meta_description',
        'meta_keyword',
        'og_title',
        'og_description',
        'hero_description',
        'hero_h1_title',
        'hero_button_title',
        'our_services_title',
        'our_services',
        'our_services_btn_title',
        'about_as_title',
        'about_us_paragraphs',
        'about_us_you_can_save_prefix',
        'about_us_you_can_save_percent',
        'about_us_you_can_save_text',
        'about_as_img_title',
        'about_as_img_alt',
        'running_text',
        'outstaffing_benefits_title',
        'outstaffing_benefits_items',
        'hire_a_team_title',
        'hire_a_team_description',
        'hire_a_team_btn_title',
        'partners',
        'form_title',
        'form_description',
        'our_partners_title',

    ];

    protected $fillable = [
        'meta_title',
        'meta_description',
        'meta_keyword',
        'og_title',
        'og_description',
        'og_img',
        'hero_description',
        'hero_h1_title',
        'hero_button_title',
        'our_services_title',
        'our_services',
        'our_services_btn_title',
        'about_as_title',
        'about_us_paragraphs',
        'about_us_you_can_save_prefix',
        'about_us_you_can_save_percent',
        'about_us_you_can_save_text',
        'about_as_img',
        'about_as_img_title',
        'about_as_img_alt',
        'white_logo',
        'running_text',
        'outstaffing_benefits_title',
        'outstaffing_benefits_items',
        'hire_a_team_title',
        'hire_a_team_description',
        'hire_a_team_btn_title',
        'partners',
        'form_title',
        'form_description',
        'our_partners_title',
        'technology_amount',
        'about_as_button_link',
    ];

    public $mediaToUrl = [
        'og_img',
        'our_services',
        'img',
        'about_as_img',
        'white_logo',
        'partners',
        'partners_logo',
    ];

    public function specialists() {
        return $this->belongsToMany(Specialist::class, 'main_specialists');
    }

    public static function normalizeData($object){
        self::getNormalizedField($object, 'our_services', 'item_title', true, true);
        self::getNormalizedField($object, 'about_us_paragraphs','paragraph_content',true,true);
        self::getNormalizedField($object, 'outstaffing_benefits_items','item_title',true,true);
        self::getNormalizedField($object, 'partners','partners_logo',true,true);

        if (array_key_exists('partners', $object)){
            foreach ($object['partners'] as $partner){
                $partners[] = $partner['partners_logo'];
            }
        }
        $object['partners'] = [
            'title' => (array_key_exists('our_partners_title', $object) ? $object['our_partners_title'] : ''),
            'logo' => $partners,
        ];

//        foreach ($object['specialists'] as $key => $specialist){
//            $object['specialists'][$key]['profession'] = $specialist['profession'][0]['title'];
//        }

        return $object;
    }

    public function  getFullData(){

        try{
            $data = $this->getAllWithMediaUrlWithout(['specialist_id', 'created_at','updated_at']);
            return self::normalizeData($data);
        }catch (\Exception $ex){
            throw new ModelNotFoundException();
        }

    }
}
