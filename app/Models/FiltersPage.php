<?php

namespace App\Models;

use App\Traits\TranslateAndConvertMediaUrl;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class FiltersPage extends Model
{
    use HasFactory, TranslateAndConvertMediaUrl;

    protected $fillable = [
        'status_id',
        'profession_id',
        'specialization_id',
        'slug',
        'experience_id',
        'english_level_id',
        'technology_id',
        'technology_two_id',
        'meta_title',
        'h1',
        'meta_description',
        'seo_text'
    ];

    public function profession(){
        return $this->belongsTo(Profession::class);
    }
    public function specialization(){
        return $this->belongsTo(Specialization::class);
    }
    public function experience(){
        return $this->belongsTo(Experience::class);
    }
    public function englishLevel(){
        return $this->belongsTo(EnglishLevel::class);
    }
    public function technology(){
        return $this->belongsTo(Technology::class);
    }
    public function technologyTwo(){
        return $this->belongsTo(Technology::class, 'technology_two_id');
    }

    public static function normalizeData($object){
        return $object;
    }

    public function getFullData(){
        try{
            $data = $this->getAllWithMediaUrlWithout(['id', 'created_at','updated_at']);
            return self::normalizeData($data);
        }catch (\Exception $ex){
            throw new ModelNotFoundException();
        }
    }
}
