<?php

namespace App\Models;

use App\Models\GlobalScopes\AlphabetScope;
use App\Traits\SeoTexts;
use App\Traits\TranslateAndConvertMediaUrl;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spatie\Translatable\HasTranslations;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

/**
 * @method static leftJoin(string $string, string $string1, string $string2, string $string3)
 */
class Technology extends Model implements Sortable
{
    use HasFactory, HasTranslations, TranslateAndConvertMediaUrl, SortableTrait, SeoTexts;

    protected $fillable = [
        'title',
        'sort_order',
        'specialization_id'
    ];

    public $translatable = [

    ];

    public $sortable = [
        'order_column_name' => 'sort_order',
        'sort_when_creating' => true,
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'technologies';

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new AlphabetScope());
    }


    public function specialists() {
        return $this->belongsToMany(Specialist::class, 'specialist_technology');
    }

    public function professions() {
        return $this->belongsToMany(Profession::class, 'technology_profession');
    }

    public function specializations(){
        return $this->belongsToMany(Specialization::class, 'specialization_technology');
    }

    public function fieldsForGenerator(){
        return $this->select('id', 'title as name');
    }

    public static function normalizeData($object){
        return $object;
    }

    public function getFullData(){
        try{
            $data = $this->getAllWithMediaUrlWithout(['created_at','updated_at']);
            return $data;
        }catch (\Exception $ex){
            throw new ModelNotFoundException();
        }
    }
}
