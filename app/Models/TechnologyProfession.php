<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class TechnologyProfession extends Model
{
    use HasFactory, HasTranslations;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'technology_profession';


    protected $fillable = [
        'technology_id',
        'profession_id',
    ];

    public $translatable = [

    ];

    public function professions() {
        return $this->BelongsToMany(Profession::class, 'technology_profession')->withPivotValue('profession_id');
    }
}
