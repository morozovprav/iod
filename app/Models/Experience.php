<?php

namespace App\Models;

use App\Traits\SeoTexts;
use App\Traits\TranslateAndConvertMediaUrl;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Spatie\Translatable\HasTranslations;

class Experience extends Model implements Sortable
{
    use HasFactory, HasTranslations, TranslateAndConvertMediaUrl, SortableTrait, SeoTexts;

    public $sortable = [
        'order_column_name' => 'sort_order',
        'sort_when_creating' => true,
    ];

    protected $fillable = [
        'title',
        'info'
    ];

    public $translatable = [
        'info',
    ];

    public function specialistSpecializations() {
        return $this->hasMany(SpecialistSpecialization::class);
    }

    public static function normalizeData($object){
        return $object;
    }

    public function fieldsForGenerator(){
        return $this->select('id', 'title as name');
    }

    public function getFullData(){
        try{
            $data = $this->getAllWithMediaUrlWithout(['created_at','updated_at']);
            return self::normalizeData($data);
        }catch (\Exception $ex){
            throw new ModelNotFoundException();
        }
    }
}


