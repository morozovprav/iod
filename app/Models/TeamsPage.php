<?php

namespace App\Models;

use App\Traits\TranslateAndConvertMediaUrl;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spatie\Translatable\HasTranslations;

class TeamsPage extends Model
{
    use HasFactory, HasTranslations,TranslateAndConvertMediaUrl;

    protected $translatable = [
        'meta_title',
        'meta_description',
        'meta_keyword',
        'og_title',
        'og_description',
        'hero_h1_title',
        'hero_paragraph',
        'why_hire_block_title',
        'why_hire_left_paragraphs',
        'why_hire_mark_list',
        'why_hire_right_paragraph',
        'consolidate_title',
        'consolidate_right_paragraphs',
        'consolidate_mark_list',
        'advantage_block_title',
        'advantage',
        'how_we_work_block_title',
        'how_we_work_left_paragraph',
        'how_we_work_right_paragraphs',
        'hire_a_team_for_your_task_title',
        'hire_a_team_for_your_task_description',
        'form_title',
        'form_description',
        'team_block_composition_label',
        'team_block_description_label',
        'team_block_button_title',
        'team_block_members_label',
        'popup_title',
        'popup_description',
        'popup_btn_title',
    ];
    protected $fillable = [
        'meta_title',
        'meta_description',
        'meta_keyword',
        'og_title',
        'og_description',
        'og_img',
        'hero_h1_title',
        'hero_paragraph',
        'hero_img',
        'hero_img_title',
        'hero_img_alt',
        'why_hire_block_title',
        'why_hire_left_paragraphs',
        'why_hire_mark_list',
        'why_hire_right_paragraph',
        'why_hire_img',
        'why_hire_img_title',
        'why_hire_img_alt',
        'consolidate_title',
        'consolidate_img',
        'consolidate_img_title',
        'consolidate_img_alt',
        'consolidate_right_paragraphs',
        'consolidate_mark_list',
        'advantage_block_title',
        'advantage',
        'how_we_work_block_title',
        'how_we_work_img',
        'how_we_work_img_title',
        'how_we_work_img_alt',
        'how_we_work_left_paragraph',
        'how_we_work_right_paragraphs',
        'hire_a_team_for_your_task_title',
        'hire_a_team_for_your_task_description',
        'form_title',
        'form_description',
        'team_block_composition_label',
        'team_block_description_label',
        'team_block_button_title',
        'team_block_members_label',
        'popup_title',
        'popup_description',
        'popup_btn_title',

    ];
    public $mediaToUrl = [
        'og_img',
        'hero_img',
        'why_hire_img',
        'consolidate_img',
        'how_we_work_img',
    ];

    public static function normalizeData($object){
        self::getNormalizedField($object, 'why_hire_left_paragraphs', 'paragraph', true, true);
        self::getNormalizedField($object, 'why_hire_mark_list', 'item_description', true, true);
        self::getNormalizedField($object, 'consolidate_right_paragraphs', 'paragraph', true, true);
        self::getNormalizedField($object, 'consolidate_mark_list', 'item_description', true, true);
        self::getNormalizedField($object, 'advantage', 'item_title', true, true);
        self::getNormalizedField($object, 'how_we_work_right_paragraphs', 'title', true, true);

        return $object;
    }

    public function  getFullData(){

        try{
            $data = $this->getAllWithMediaUrlWithout(['id','created_at','updated_at']);
            $data = self::normalizeData($data);
            return $data;
        }catch (\Exception $ex){
            throw new ModelNotFoundException();
        }
    }
}
