<?php

namespace App\Models;

use App\Traits\SeoTexts;
use App\Traits\TranslateAndConvertMediaUrl;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spatie\Translatable\HasTranslations;

class Profession extends Model
{
    use HasFactory, HasTranslations, TranslateAndConvertMediaUrl, seoTexts;

    protected $fillable = [
        'title',
    ];

    public $translatable = [
        'title',
        'plural_title',
    ];

    public function specialists() {
        return $this->hasMany(Specialist::class);
    }

    public function technologies()
    {
        return $this->belongsToMany(Technology::class, 'technology_profession');
    }

    public function specialization(){
        return $this->hasMany(Specialization::class);
    }

    public function fieldsForGenerator(){
        return $this->select('title->en as name', 'id');
    }

    public static function normalizeData($object){
        return $object;
    }

    public function getFullData(){
        try{
            $data = $this->getAllWithMediaUrlWithout(['created_at','updated_at']);
            return $data;
        }catch (\Exception $ex){
            throw new ModelNotFoundException();
        }
    }
}
