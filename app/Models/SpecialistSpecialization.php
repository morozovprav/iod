<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Relations\Pivot;

class SpecialistSpecialization extends Pivot
{
    use HasFactory, HasTranslations;

    protected $table = 'specialist_specialization';

    protected $fillable = [
        'specialist_id',
        'specialization_id',
        'experience_id',
    ];

    public $translatable = [

    ];

    public function experience(){
        return $this->belongsTo(Experience::class, 'experience_id');
    }

}
