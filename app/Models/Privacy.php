<?php

namespace App\Models;

use App\Traits\TranslateAndConvertMediaUrl;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spatie\Translatable\HasTranslations;

class Privacy extends Model
{
    use HasFactory, HasTranslations,TranslateAndConvertMediaUrl;

    protected $translatable = [
        'meta_title',
        'meta_description',
        'meta_keyword',
        'og_title',
        'og_description',
        'h1_title',
        'blocks',
    ];
    protected $fillable = [
        'meta_title',
        'meta_description',
        'meta_keyword',
        'og_title',
        'og_description',
        'og_img',
        'h1_title',
        'blocks',
    ];
    public $mediaToUrl = [

    ];

    public static function normalizeData($object){
        self::getNormalizedField($object, 'blocks', 'element', true, true);

        foreach ($object['blocks'] as $blockKey => $item){
            if (array_key_exists('paragraph', $item)){
            continue;
        }
            foreach ($item['element'] as $elemKey => $element){

                if (array_key_exists('mark_list_item', $element['attributes'])){

                    foreach ($element['attributes']['mark_list_item'] as $listItem){
                        $listItemResult[] = $listItem['attributes'];
                    }
                    $elements[] = ['mark_list_item' => $listItemResult];
                } else {
                    $elements[] = $element['attributes'];
                }

            }
            $object['blocks'][$blockKey]['element'] = $elements;
        }
        return $object;
    }

    public function  getFullData(){

        try{
            $data = $this->getAllWithMediaUrlWithout(['id','created_at','updated_at']);
            $data = self::normalizeData($data);
            return $data;
        }catch (\Exception $ex){
            throw new ModelNotFoundException();
        }

    }
}
