<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpecialistTechnology extends Model
{
    use HasFactory;


    protected $fillable = [
        'specialist_id',
        'technology_id',
    ];
}
