<?php

namespace App\Models;

use App\Traits\TranslateAndConvertMediaUrl;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spatie\Translatable\HasTranslations;

class SpecialistPage extends Model
{
    use HasFactory, HasTranslations,TranslateAndConvertMediaUrl;

    public $translatable = [
        'specialization_label',
        'work_experience_label',
        'english_level_label',
        'technoligy_label',
        'project_label',
        'certificate_label',
        'courses_label',
        'teams_label',
        'other_collegues_label',
        'spec_buton_title',
        'teams_buton_title',
        'popup_title',
        'popup_description',
        'popup_btn_title'
    ];
    protected $fillable = [
        'specialization_label',
        'work_experience_label',
        'english_level_label',
        'technoligy_label',
        'project_label',
        'certificate_label',
        'courses_label',
        'teams_label',
        'other_collegues_label',
        'spec_buton_title',
        'teams_buton_title',
        'popup_title',
        'popup_description',
        'popup_btn_title'
    ];
    public function  getFullData(){

        try{
            $data = $this->getAllWithMediaUrlWithout(['id','created_at','updated_at']);
            return $data;
        }catch (\Exception $ex){
            throw new ModelNotFoundException();
        }

    }
}
