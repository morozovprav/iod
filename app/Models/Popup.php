<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Popup extends Model
{
    use HasFactory;
    public $translatable = [

        ];
    protected  $fillable = [
        'name',
        'phone',
        'mail',
        'message',
        'file',
    ];
}
