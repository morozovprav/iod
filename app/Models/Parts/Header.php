<?php

namespace App\Models\Parts;

use App\Traits\TranslateAndConvertMediaUrl;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spatie\Translatable\HasTranslations;

class Header extends Model
{
    use HasFactory, HasTranslations,TranslateAndConvertMediaUrl;

    protected $translatable = [
        'specialists_label',
        'specialists_link',
        'pages_link',
        'login_button_label',
        'contact_us_label'
    ];
    protected $fillable = [
        'logo',
        'specialists_label',
        'specialists_link',
        'pages_link',
        'login_button_label',
        'contact_us_label',
        'what_to_display'
    ];
    public $mediaToUrl = [
        'logo',
    ];

    public static function normalizeData($object){

        self::getNormalizedField($object, 'pages_link', 'link_title', true, true);
        return $object;
    }

    public function  getFullData(){

        try{
            $data = $this->getAllWithMediaUrlWithout(['id','created_at','updated_at']);
            $data = self::normalizeData($data);
            return $data;
        }catch (\Exception $ex){
            throw new ModelNotFoundException();
        }

    }
}
