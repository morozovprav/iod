<?php

namespace App\Models\Parts;

use App\Traits\TranslateAndConvertMediaUrl;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spatie\Translatable\HasTranslations;

class Footer extends Model
{
    use HasFactory, HasTranslations,TranslateAndConvertMediaUrl;

    protected $translatable = [
        'copyright',
        'privacy_label',
        'pages_link',
    ];
    protected $fillable = [
        'copyright',
        'privacy_label',
        'privacy_link',
        'pages_link',
    ];
    public $mediaToUrl = [

    ];

    public static function normalizeData($object){

        self::getNormalizedField($object, 'pages_link', 'link_title', true, true);
        return $object;
    }

    public function  getFullData(){

        try{
            $data = $this->getAllWithMediaUrlWithout(['id','created_at','updated_at']);
            $data = self::normalizeData($data);
            return $data;
        }catch (\Exception $ex){
            throw new ModelNotFoundException();
        }

    }
}
