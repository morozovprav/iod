<?php

namespace App\Models;

use App\Traits\TranslateAndConvertMediaUrl;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spatie\Translatable\HasTranslations;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

class Team extends Model implements Sortable
{
    use HasFactory, HasTranslations,TranslateAndConvertMediaUrl, SortableTrait;

    public $sortable = [
        'order_column_name' => 'sort_order',
        'sort_when_creating' => true,
    ];

    protected $translatable = [
        'title_task',
        'short_title',
        'team_composition',
        'description',
    ];
    protected $fillable = [
        'title_task',
        'short_title',
        'team_composition',
        'description',
    ];
    public $mediaToUrl = [

    ];

    public function specialists(){
        return $this->belongsToMany(Specialist::class, 'specialist_team', 'team_id', 'specialist_id');
    }

    public function scopeAddManualSort($q){
        return $q->orderBy('sort_order');
    }

    public static function normalizeData($object){
        return $object;
    }

    public function getOneTeam(){
        return $this->with('specialists', function ($q){
            $q->select('name', 'main_img', 'profession_id', 'status_id', 'slug', 'specialists.id', 'english_level_id')
                ->with('status', 'specializations', 'englishLevel')
                ->with('profession', function ($q){
                    $q->select('title', 'id');
                })
                ->withCount('teams');
        });
    }

    public function getFullData(){

        try{
            $data = $this->getAllWithMediaUrlWithout(['created_at','updated_at']);
            $data = self::normalizeData($data);
            return $data;
        }catch (\Exception $ex){
            throw new ModelNotFoundException();
        }

    }
}
