<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MainSpecialist extends Model
{

    use HasFactory;


    public function specialists() {
        return $this->BelongsToMany(Specialist::class, 'main_specialists')->withPivotValue('main_id');
    }
}
