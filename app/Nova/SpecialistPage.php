<?php

namespace App\Nova;

use Digitalcloud\MultilingualNova\Multilingual;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class SpecialistPage extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\SpecialistPage::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Multilingual::make('language'),
            Text::make('Specialization label')->hideFromIndex(),
            Text::make('Work experience label')->hideFromIndex(),
            Text::make('English level label')->hideFromIndex(),
            Text::make('Technoligy label')->hideFromIndex(),
            Text::make('Project label')->hideFromIndex(),
            Text::make('Certificate label')->hideFromIndex(),
            Text::make('Courses label')->hideFromIndex(),
            Text::make('Teams label')->hideFromIndex(),
            Text::make('Other collegues label')->hideFromIndex(),
            Text::make('Spec buton title')->hideFromIndex(),
            Text::make('Teams buton title')->hideFromIndex(),
            Text::make('Popup title')->hideFromIndex(),
            Text::make('Popup description')->hideFromIndex(),
            Text::make('Popup btn title')->hideFromIndex(),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
