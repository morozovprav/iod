<?php

namespace App\Nova;

use ClassicO\NovaMediaLibrary\MediaLibrary;
use Digitalcloud\MultilingualNova\Multilingual;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Whitecube\NovaFlexibleContent\Flexible;

class About extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\About::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Multilingual::make('language'),
            Text::make('Hero h1 title')->hideFromIndex(),
            MediaLibrary::make('Hero img')->hideFromIndex(),
            Text::make('Hero description left')->hideFromIndex(),
            Flexible::make('Hero description botom')
            ->addLayout('One paragraph','one_paragraph',[
                Text::make('paragraph'),
            ])
                ->button('Add paragraph')
                ->hideFromIndex(),
            Flexible::make('Advantage')
            ->addLayout('Advantage','advantage',[
                Text::make('Item title'),
                Text::make('Item description'),
                MediaLibrary::make('Video')
                ])
                ->button('Add Advantage')
                ->hideFromIndex(),
            Text::make('Numeric list title', 'numeric_list_title'),
            Flexible::make('Numeric list')
                ->addLayout('One item','one_item',[
                    Text::make('Item description'),
                ])
                ->button('Add Numeric')
                ->hideFromIndex(),
            Text::make('Mark list title', 'mark_list_title'),
            Flexible::make('Mark list')
                ->addLayout('Mark','mark_list',[
                    Text::make('Item description'),
                ])
                ->button('Add Mark')
                ->hideFromIndex(),
            Flexible::make('Paragraphs')
                ->addLayout('Paragraphs','paragraphs',[
                    Text::make('Paragraph')
                ])
                ->button('Add Paragraph')
                ->hideFromIndex(),
            Flexible::make('Galery')
                ->addLayout('Galery','galery',[
                    MediaLibrary::make('Img'),
                    Text::make('Img title'),
                    Text::make('Img alt')
                ])
                ->button('Add image')
                ->hideFromIndex(),

            Flexible::make('Advantage botom')
                ->addLayout('Advantage botom','advantage_botom',[
                    Text::make('Item title'),
                    Text::make('Item description'),
                    MediaLibrary::make('Img'),
                    Text::make('Img title'),
                    Text::make('Img alt')
                ])
                ->button('Add Advantage')
                ->hideFromIndex(),
            Text::make('Meta title')->hideFromIndex(),
            Text::make('Meta description')->hideFromIndex(),
            Text::make('Meta keyword')->hideFromIndex(),
            Text::make('Og title')->hideFromIndex(),
            Text::make('Og description')->hideFromIndex(),
            MediaLibrary::make('Og img')->hideFromIndex(),


        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
