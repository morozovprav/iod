<?php

namespace App\Nova;

use ClassicO\NovaMediaLibrary\MediaLibrary;
use Digitalcloud\MultilingualNova\Multilingual;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use Whitecube\NovaFlexibleContent\Flexible;

class Privacy extends Resource
{
    public static function label(){
        return 'Privacy';
    }
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Privacy::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Multilingual::make('Language'),

            Text::make('Meta-title', 'meta_title')->hideFromIndex(),
            Text::make('Meta-description', 'meta_description')->hideFromIndex(),
            Text::make('Meta-keywords', 'meta_keyword')->hideFromIndex(),

            Text::make('Og title', 'og_title')->hideFromIndex(),
            Text::make('Og description', 'og_description')->hideFromIndex(),
            MediaLibrary::make('Og image', 'og_img')->hideFromIndex(),

            Text::make('H1 title'),

            Flexible::make('Blocks')
            ->addLayout('Paragraph', 'paragraph', [
                Textarea::make('paragraph')
            ])
            ->addLayout('Block', 'block', [
                Text::make('Block title'),
                Flexible::make('Element', 'element')
                    ->addLayout('H3 title + paragraph', 'h3_title_paragraph', [
                        Text::make('h3_title'),
                        Textarea::make('paragraph')
                    ])
                    ->addLayout('Paragraph', 'paragraph', [
                        Textarea::make('paragraph')
                    ])
                    ->addLayout('Paragraph + link', 'paragraph_link', [
                        Textarea::make('Paragraph'),
                        Text::make('Link'),
                    ])
                    ->addLayout('Mark list', 'mark_list', [
                        Flexible::make('Mark list item', 'mark_list_item')
                        ->addLayout('Item', 'item', [
                            Text::make('Mark item', 'mark_item'),
                        ])
                    ])
            ])

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
