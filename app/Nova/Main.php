<?php

namespace App\Nova;

use ClassicO\NovaMediaLibrary\MediaLibrary;
use Digitalcloud\MultilingualNova\Multilingual;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\HasManyThrough;
use Laravel\Nova\Fields\Heading;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use NovaAttachMany\AttachMany;
use Whitecube\NovaFlexibleContent\Flexible;


class Main extends Resource
{
    public static function label(){
        return 'Main page';
    }
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Main::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Multilingual::make('language'),
            Text::make('Title')->hideFromIndex(),
            Text::make('Description')->hideFromIndex(),
            Text::make('H1 text', 'h1_text')->hideFromIndex(),
            Text::make('H2 text', 'h2_text')->hideFromIndex(),

            Text::make('See all button text', 'see_all_btn_txt')->hideFromIndex(),
            Text::make('See all button url', 'see_all_btn_url')
                ->withMeta(['extraAttributes' => [
                    'readonly' => true
                ]])
                ->default('/specialists_page')
                ->hideFromIndex(),

            AttachMany::make('Specialists', 'specialists', 'App\Nova\Specialist'),
            Heading::make('SEO'),
            Text::make('Meta title')->hideFromIndex(),
            Text::make('Meta description')->hideFromIndex(),
            Text::make('Meta keyword')->hideFromIndex(),
            Text::make('Og title')->hideFromIndex(),
            Text::make('Og description')->hideFromIndex(),
            MediaLibrary::make('Og img')->hideFromIndex(),
            Heading::make('Hero block'),
            Text::make('Hero description')->hideFromIndex(),
            Text::make('Hero h1 title')->hideFromIndex(),
            Text::make('Hero button title')->hideFromIndex(),
            Heading::make('Catalog block'),
            Text::make('Technology amount')
                ->rules('integer')
                ->hideFromIndex(),
            Heading::make('Our services'),
            Text::make('Our services title')->hideFromIndex(),
            Flexible::make('Our services')
                ->addLayout('Service','service',[
                    Text::make('Item title'),
                    Textarea::make('Item description'),
                    MediaLibrary::make('Img'),
                    Text::make('Img title'),
                    Text::make('Img alt'),
                    Text::make('Link'),
                    Text::make('Btn title'),
                ])
                ->button('Add service')
                ->hideFromIndex(),
            Text::make('Our services btn title')->hideFromIndex(),
            Heading::make('About as'),
            Text::make('About as title')->hideFromIndex(),
            //TODO установить лимит на нижестоящий флекс. При попытке поле становится недоступным для редактирования. Пропадает. Разобраться.
            Flexible::make('About us paragraphs')
                ->addLayout('Paragraph','paragraph',[
                    Textarea::make('Paragraph content'),
                ])
                ->button('Add paragraph')
                ->hideFromIndex(),
            Text::make('About us You can save prefix')->hideFromIndex(),
            Text::make('About us You can save percent')->hideFromIndex(),
            Text::make('About us You can save text')->hideFromIndex(),
            MediaLibrary::make('About as Img')
                ->rules('required')
                ->hideFromIndex(),
            Text::make('About as img title')->hideFromIndex(),
            Text::make('About as Img alt')->hideFromIndex(),
            Text::make('About as button link')->hideFromIndex(),
            Heading::make('Running text')->hideFromIndex(),
            MediaLibrary::make('White logo')
                ->rules('required')
                ->hideFromIndex(),
            Text::make('Running text')->hideFromIndex(),
            Heading::make('Outstaffing benefits')->hideFromIndex(),
            Text::make('Outstaffing benefits title')->hideFromIndex(),
            Flexible::make('Outstaffing benefits items')
                ->addLayout('One item','one_item',[
                    Text::make('Item title'),
                    Textarea::make('Item description'),
                ])
                ->button('Add item')
                ->hideFromIndex(),
            Heading::make('Hire a team'),
            Text::make('Hire a team title')->hideFromIndex(),
            Text::make('Hire a team description')->hideFromIndex(),
            Text::make('Hire a team btn title')->hideFromIndex(),
            Heading::make('Our partners'),
            Text::make('Our partners title')->rules('required')->hideFromIndex(),
            Flexible::make('Partners')
                ->addLayout('One item','one_item',[
                    MediaLibrary::make('Partners logo')
                ])
                ->button('Add partner')
                ->hideFromIndex(),
            Heading::make('Form'),
            Text::make('Form title')->hideFromIndex(),
            Text::make('Form description')->hideFromIndex(),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
