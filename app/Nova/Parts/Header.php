<?php

namespace App\Nova\Parts;

use App\Nova\Resource;
use ClassicO\NovaMediaLibrary\MediaLibrary;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Whitecube\NovaFlexibleContent\Flexible;

class Header extends Resource
{
    public static function label() {
        return 'Header';
    }

    public static $group = 'Parts';
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Parts\Header::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            MediaLibrary::make('Logo'),
            Text::make('Specialists label'),
            Text::make('Specialists link')
//                ->withMeta(['extraAttributes' => [
//                    'readonly' => true
//                ]])
                ->default('/specialists')
            ,
            Flexible::make('Pages link')
                ->addLayout('One link', 'óne_link', [
                    Text::make('Link title'),
                    Text::make('Link'),
//                    Select::make('Pages link')->options($this->links)
                ])
                ->limit(3)
                ->button('Add link'),
            Text::make('Login button label'),
            Text::make('Contact us label'),
            Select::make('What to show on spec card?', 'what_to_display')->options([
                'english-level' => 'English level',
                'technologies' => 'Technologies',
            ])->default('english-level')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
