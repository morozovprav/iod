<?php

namespace App\Nova;

use ClassicO\NovaMediaLibrary\MediaLibrary;
use Digitalcloud\MultilingualNova\Multilingual;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use Whitecube\NovaFlexibleContent\Flexible;

class Contact extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Contact::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Multilingual::make('language'),
            Text::make('H1 title')->hideFromIndex(),
            Text::make('Work hours label')->hideFromIndex(),
            Text::make('Work hours')->hideFromIndex(),
            Text::make('Work hours time zone city')->hideFromIndex(),
            Text::make('Office address label')->hideFromIndex(),
            Textarea::make('Office address')->hideFromIndex(),
            Text::make('Email label')->hideFromIndex(),
            Text::make('Email')
                ->hideFromIndex()
                ->rules('email'),
            Text::make('Phone label')->hideFromIndex(),
            Text::make('Phone')->hideFromIndex(),
            Text::make('Social')->hideFromIndex(),
            Text::make('Form name placeholder')->hideFromIndex(),
            Text::make('Form email placeholder')->hideFromIndex(),
            Text::make('Form phone placeholder')->hideFromIndex(),
            Text::make('Form textarea placeholder')->hideFromIndex(),
            Text::make('Form privacy first part label')->hideFromIndex(),
            Text::make('Form privacy second part label')->hideFromIndex(),
            Text::make('Form file label')->hideFromIndex(),
            Text::make('Form button title')->hideFromIndex(),

            Text::make('Thank you title')->hideFromIndex(),
            Text::make('Thank you description')->hideFromIndex(),
            Text::make('Thank you btn title')->hideFromIndex(),

            Text::make('Meta title')->hideFromIndex(),
            Textarea::make('Meta description')->hideFromIndex(),
            Text::make('Meta keyword')->hideFromIndex(),
            Text::make('Og title')->hideFromIndex(),
            Textarea::make('Og description')->hideFromIndex(),
            MediaLibrary::make('Og img')->hideFromIndex(),
            Text::make('Messengers label')->hideFromIndex(),
            Flexible::make('Messengers')
                ->addLayout('One massager', 'one_massager', [
                    Select::make('Messenger title')->options([
                        'telegram' => 'telegram',
                        'instagram' => 'instagram',
                    ]),
                    Text::make('Messenger link'),
                ])
                ->hideFromIndex(),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
