<?php

namespace App\Nova;


use ClassicO\NovaMediaLibrary\MediaLibrary;
use Digitalcloud\MultilingualNova\Multilingual;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Slug;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use OptimistDigital\MultiselectField\Multiselect;
use Waynestate\Nova\CKEditor;
use Wehaa\LiveupdateBoolean\LiveupdateBoolean;
use Whitecube\NovaFlexibleContent\Flexible;

class Specialist extends Resource
{
    public static $group = 'Members';

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Specialist::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name',
    ];
//
//    protected $attributes = [
//        'display' => true,
//    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Multilingual::make('Language'),
            //TODO сделать отображение уровня навыка в блоке связанных ресурсов в ресурсе специалиста
            BelongsTo::make('Status', 'status', 'App\Nova\Status')
                ->hideFromIndex()
                ->nullable(),
            BelongsToMany::make('Specialization', 'specializations', 'App\Nova\Specialization')
            ->fields(function() {
                return [
                    Select::make('Уровень', 'experience_id')->options(
                        \App\Models\Experience::all()->pluck('title', 'id')
                    )
                ];
            }),
            BelongsToMany::make('Technologies', 'technologies', 'App\Nova\Technology'),

            Text::make('Meta-title', 'meta_title')->hideFromIndex(),
            Text::make('Meta-description', 'meta_description')->hideFromIndex(),
            Text::make('Meta-keywords', 'meta_keyword')->hideFromIndex(),

            Text::make('Og title', 'og_title')->hideFromIndex(),
            Text::make('Og description', 'og_description')->hideFromIndex(),
            MediaLibrary::make('Og image', 'og_img')->hideFromIndex(),

//            LiveupdateBoolean::make('Display in site?', 'display'),
            (strpos($request->fullUrl(), '/specialists/') !== false
                // Use plain checkbox in the dedicated form
                ? Boolean::make('Display in site?', 'display')->default(1)
                // Use inline checkbox in table view
                : LiveupdateBoolean::make('Display in site?', 'display')->hideFromDetail(true)),

            Text::make('Name', 'name')->rules('required'),
            Slug::make('Slug')->from('name')->separator('-')->creationRules('unique:specialists'),
            MediaLibrary::make('Photo', 'main_img')->hideFromIndex(),

            Textarea::make('About me', 'about_me')->hideFromIndex(),
            Text::make('Portfolio link', 'portfolio_link')->hideFromIndex(),
            Flexible::make('Projects', 'projects')->hideFromIndex()
                ->addLayout('One project', 'one_project', [
                    Text::make('Title'),
                    Text::make('Years'),
                    CkEditor::make('Description'),
                    Text::make('Technologies'),
//                    Multiselect::make('Technologies', 'technologies')
//                        ->options(\App\Models\Technology::all()->pluck('title', 'title'))
//                        ->saveAsJSON()

                ])->button('Add project'),
            Flexible::make('Certificates', 'certificates')->hideFromIndex()
                ->addLayout('One certificate', 'one_certificate', [
                    Text::make('Title'),
                    MediaLibrary::make('Img', 'img')->hideFromIndex(),
                    Text::make('Link'),
                ])->button('Add certificate'),
            Flexible::make('Courses', 'courses')->hideFromIndex()
                ->addLayout('One course', 'one_course', [
                    Text::make('Title'),
                    Text::make('Description'),
                ])->button('Add course'),
            BelongsTo::make('English level', 'englishLevel', 'App\Nova\EnglishLevel')
                ->hideFromIndex()
                ->nullable(),
            Boolean::make('English is checked')->hideFromIndex(),
            BelongsTo::make('Profession', 'profession', 'App\Nova\Profession')
                ->hideFromIndex()
                ->nullable(),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
