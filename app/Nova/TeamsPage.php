<?php

namespace App\Nova;

use ClassicO\NovaMediaLibrary\MediaLibrary;
use Digitalcloud\MultilingualNova\Multilingual;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use Whitecube\NovaFlexibleContent\Flexible;

class TeamsPage extends Resource
{
    public static function label(){
        return 'Teams page';
    }
    public static $group = 'Teams';
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\TeamsPage::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Multilingual::make('Language'),

            Text::make('Meta title')->hideFromIndex(),
            Text::make('Meta description')->hideFromIndex(),
            Text::make('Meta keyword')->hideFromIndex(),
            Text::make('Og title')->hideFromIndex(),
            Text::make('Og description')->hideFromIndex(),
            MediaLibrary::make('Og img')->hideFromIndex(),

            Text::make('Hero h1 title'),
            Text::make('Hero paragraph')->hideFromIndex(),
            MediaLibrary::make('Hero img')->hideFromIndex(),
            Text::make('Hero img title')->hideFromIndex(),
            Text::make('Hero img alt')->hideFromIndex(),

            Text::make('Why hire block title')->hideFromIndex(),
            Flexible::make('Why hire left paragraphs')->hideFromIndex()
                ->addLayout('One paragraph','one_paragraph',[
                    Textarea::make('paragraph'),
                ])
                ->button('Add paragraph')
                ->hideFromIndex(),
            Flexible::make('Why hire mark list')->hideFromIndex()
                ->addLayout('Mark item','mark_item',[
                    Textarea::make('Item description'),
                ])
                ->button('Add Mark item')
                ->hideFromIndex(),
            Textarea::make('Why hire right paragraph')->hideFromIndex(),
            MediaLibrary::make('Why hire img')->hideFromIndex(),
            Text::make('Why hire img title')->hideFromIndex(),
            Text::make('Why hire img alt')->hideFromIndex(),

            Text::make('Consolidate title')->hideFromIndex(),
            MediaLibrary::make('Consolidate img')->hideFromIndex(),
            Text::make('Consolidate img title')->hideFromIndex(),
            Text::make('Consolidate img alt')->hideFromIndex(),
            Flexible::make('Consolidate right paragraphs')->hideFromIndex()
                ->addLayout('One paragraph','one_paragraph',[
                    Text::make('paragraph'),
                ])
                ->limit(2)
                ->button('Add paragraph')
                ->hideFromIndex(),
            Flexible::make('Consolidate mark list')->hideFromIndex()
                ->addLayout('Mark item','mark_item',[
                    Text::make('Item description'),
                ])
                ->button('Add Mark item')
                ->hideFromIndex(),

            Text::make('Advantage block title')->hideFromIndex(),
            Flexible::make('Advantage')->hideFromIndex()
                ->addLayout('Advantage','advantage',[
                    Text::make('Item title'),
                ])
                ->button('Add Advantage')
                ->hideFromIndex(),

            Text::make('How we work block title')->hideFromIndex(),
            MediaLibrary::make('How we work img')->hideFromIndex(),
            Text::make('How we work img title')->hideFromIndex(),
            Text::make('How we work img alt')->hideFromIndex(),
            Text::make('How we work left paragraph')->hideFromIndex(),
            Flexible::make('How we work right paragraphs')->hideFromIndex()
                ->addLayout('One paragraph','one_paragraph',[
                    Text::make('title'),
                    Text::make('paragraph'),
                ])
                ->limit(4)
                ->button('Add paragraph')
                ->hideFromIndex(),

            Text::make('Hire a team for your task title')->hideFromIndex(),
            Textarea::make('Hire a team for your task description')->hideFromIndex(),
            Text::make('Team block composition label')->hideFromIndex(),
            Text::make('Team block description label')->hideFromIndex(),
            Text::make('Team block button title')->hideFromIndex(),
            Text::make('Team block members label')->hideFromIndex(),

            Text::make('Form title')->hideFromIndex(),
            Text::make('Form description')->hideFromIndex(),

            Text::make('Popup title')->hideFromIndex(),
            Text::make('Popup description')->hideFromIndex(),
            Text::make('Popup btn title')->hideFromIndex(),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
