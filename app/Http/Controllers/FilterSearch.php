<?php

namespace App\Http\Controllers;

use App\Models\EnglishLevel;
use App\Models\Profession;
use App\Models\Specialization;
use App\Models\Status;
use App\Models\Technology;
use Illuminate\Http\Request;
use ProtoneMedia\LaravelCrossEloquentSearch\Search;

class FilterSearch extends Controller
{
    protected function filterSearch(Request $request){
        $searchResultModelGroups = Search::add(EnglishLevel::class, 'title')
            ->add(Profession::class, 'title')
            ->add(Technology::class, 'title')
            ->add(Status::class, 'title')
            ->add(Specialization::class, 'title')
            ->includeModelType()
            ->beginWithWildcard()
            ->search($request->find)->groupBy('type');

        $result = [];
        foreach ($searchResultModelGroups as $groupKey => $group){
            $r = [];
            foreach ($group as $model){
                $r[] = $model->getFullData();
            }
            $result[$groupKey] = $r;
        }
        return response()->json([
            'status' => 'success',
            'result' => $result
        ]);
    }
}
