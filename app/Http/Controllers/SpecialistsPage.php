<?php

namespace App\Http\Controllers;

use App\Models\EnglishLevel;
use App\Models\Experience;
use App\Models\Filter;
use App\Models\Profession;
use App\Models\Specialization;
use App\Models\Status;
use App\Models\TeamsPage;
use App\Models\Technology;
use Illuminate\Http\Request;

class SpecialistsPage extends Controller
{
    public function index(){
        $data = \App\Models\SpecialistsPage::firstOrFail();
        $data = $data->getFullData();

//        $englishLevel = EnglishLevel::query()
//            ->whereHas('specialists')
//            ->get(['id', 'title']);
//
//        $experience = Experience::query()
//            ->whereHas('specialistSpecializations')
//            ->get(['id', 'title']);
//
//        $professions = Profession::query()
//            ->whereHas('specialists')
//            ->get(['id', 'title']);
//        foreach ($professions as $profession){
//            $prof[] = $profession->getFullData();
//        }
//
//        $technology = Technology::query()
//            ->whereHas('specialists')
//            ->orderBy('sort_order')
//            ->get(['id', 'title']);
//
//        $specializations = Specialization::query()
//            ->whereHas('specialistsForFilters')
//            ->get(['id', 'title']);
//        foreach ($specializations as $specialization){
//            $spec[] = $specialization->getFullData();
//        }
//
//
//        $statuses = Status::query()
//            ->whereHas('specialists')
//            ->get(['id', 'title']);
//        foreach ($statuses as $status){
//            $stat[] = $status->getFullData();
//        }


        $teams_title = app(Teams::class)->getTeamsTab();
        $oneTeam = app(Teams::class)->getOneTeamForTeamsPage();
        $teamsContent = TeamsPage::query()
            ->select('hire_a_team_for_your_task_title',
                'hire_a_team_for_your_task_description',
                'popup_title',
                'popup_description',
                'popup_btn_title')
            ->firstOrFail();
        $teamsContent = $teamsContent->getFullData();
        return response()->json([
           'status' => 'success',
           'data' =>  $data,

        //filters name equal filters  class name!!!
//            'filters' => [
//                'englishLevel' => $englishLevel,
//                'experience' => $experience,
//                'profession' => $prof,
//                'technology' => $technology,
//                'specialization' => $spec,
//                'status' => $stat,
//                ],
            'teams_content' => $teamsContent,
            'one_team' => $oneTeam,
            'teams_tab' => $teams_title,
        ]);
    }
}
