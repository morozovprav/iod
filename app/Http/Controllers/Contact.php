<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class Contact extends Controller
{
    public function index(){
        $data=\App\Models\Contact::firstOrFail();
        $content = $data->getFullDataForContact();

        return response()->json([
            'status'=>'success',
            'content'=>$content,
        ]);
    }
}
