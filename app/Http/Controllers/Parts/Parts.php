<?php

namespace App\Http\Controllers\Parts;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Parts\Cookie;
use App\Models\Parts\Footer;
use App\Models\Parts\Header;
use App\Models\TeamsPage;
use Illuminate\Http\Request;

class Parts extends Controller
{
    public function index() {

        $header = Header::firstOrFail();
        $header = $header->getFullData();

        $footer = Footer::firstOrFail();
        $footer = $footer->getFullData();

        $cookie = Cookie::firstOrFail();
        $cookie = $cookie->getFullData();
//        $contacts = Contact::query()->select('email', 'phone', 'messengers')->firstOrFail();
//        $contacts = $contacts->getFullData();
//        $footer = array_merge($footer, $contacts);
        $footer['logo'] = $header['logo'];

        $form = Contact::query()
            ->select('form_name_placeholder',
                'form_email_placeholder',
                'form_phone_placeholder',
                'form_textarea_placeholder',
                'form_privacy_first_part_label',
                'form_privacy_second_part_label',
                'form_file_label',
                'form_button_title',
                'email',
                'phone',
                'messengers',
                'messengers_label',
                'thank_you_title',
                'thank_you_description',
                'thank_you_btn_title',
            )
            ->firstOrFail();

        $form = $form->getFullData();

        $teamsContent = TeamsPage::query()
            ->select(
                'popup_title',
                'popup_description',
                'popup_btn_title')
            ->firstOrFail();
        $teamsContent = $teamsContent->getFullData();

        return response()->json([
           'status' => 'success',
           'footer' => $footer,
           'header' => $header,
            'cookie' => $cookie,
            'form' => $form,
            'teamsPopup' => $teamsContent,
        ]);
    }
}
