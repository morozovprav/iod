<?php

namespace App\Http\Controllers;

use App\Http\Requests\requestPopupsPost;
use App\Services\SendMailService;
use Illuminate\Http\Request;

class Popup extends Controller
{
 public function requestPopupsPost(requestPopupsPost $request) {

     $data = $request->input();
     if($request->file !== null) {
         $data['file']=$request->file->store('/');
     }else{
         $data['file']=null;
     }
     $popup = new \App\Models\Popup($data);
     $popup->save();

     SendMailService::sendEmailToAdmin('request',$data, $request);
     return response()->json([
         'status'=> 'success',
         'massage'=>'request wil be send',
     ]);



 }

}
