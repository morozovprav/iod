<?php

namespace App\Http\Controllers;

use App\Filters\SpecialistFilter;
use App\Models\EnglishLevel;
use App\Models\Experience;
use App\Models\FiltersPage;
use App\Models\Profession;
use App\Models\SeoText;
use App\Models\Specialization;
use App\Models\Status;
use App\Models\Technology;
use App\Traits\SeoParts;
use App\Traits\SeoTexts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class SeoController extends Controller
{
    use SeoParts, SeoTexts;

    public function getSlugs(){

        $specialistSlugs = \App\Models\Specialist::query()->get('slug')->pluck('slug');

        return response()->json([
            'status' => 'success',
            'specialists_slug' => $specialistSlugs
        ]);
    }

    public function getFilterSlugs(){

        $filtersSlugs = FiltersPage::query()
            ->select('id', 'slug')
            ->whereNotNull('slug')
            ->paginate(10000);

        $paginationData = [
            'all_pages' =>  $filtersSlugs->lastPage(),
            'current_page' =>  $filtersSlugs->currentPage(),
            'per_page' =>  $filtersSlugs->perPage(),
            'total' =>  $filtersSlugs->total(),
            'get_param' =>  $filtersSlugs->getOptions(),
            'next_page' =>  $filtersSlugs->nextPageUrl()
        ];

        if (count($filtersSlugs) >   1){
            foreach ($filtersSlugs as $filtersSlug){
                $res[] = $filtersSlug->getFullData();
            }
        } else {
            $res = 'You’re out of range!';
        }


        return response()->json([
            'status' => 'success',
            'filters_slug' => $res,
            'paginate_data' => $paginationData
        ]);
    }

    public function getSlugsForSeo(){
        $filtersSlugs = FiltersPage::query()
            ->select('id', 'slug')
            ->whereNotNull('slug')
            ->get();

        $page1 = $filtersSlugs->slice(0, 1001);
        $page2 = $filtersSlugs->slice(30001, 32001);
        $page3 = $filtersSlugs->slice(60001, 63001);
        $page4 = $filtersSlugs->slice(90001, 94001);
        $page5 = $filtersSlugs->slice(120001, 125001);

        $paginationData = [
            'all_pages' =>  $filtersSlugs->lastPage(),
            'current_page' =>  $filtersSlugs->currentPage(),
            'per_page' =>  $filtersSlugs->perPage(),
            'total' =>  $filtersSlugs->total(),
            'get_param' =>  $filtersSlugs->getOptions(),
            'next_page' =>  $filtersSlugs->nextPageUrl()
        ];

    }

    public function getMeta(Request $request){
        $queryParams = $request->query();
        $professionId = $this->getQueryParam($queryParams, 'profession');
        $specializationId = $this->getQueryParam($queryParams, 'specialization');
        $experienceId = $this->getQueryParam($queryParams, 'experience');
        $englishLevelId = $this->getQueryParam($queryParams, 'englishLevel');

        $technologies = $this->getQueryParamTehnologies($queryParams, 'technology');
        $technologyId = $technologies ? $technologies[0] : $technologies;
        $technologyTwoId = $technologies ? array_key_exists(1, $technologies)?$technologies[1]: null : null;

        $metaData = FiltersPage::query()
            ->select('meta_title', 'meta_description', 'meta_keyword', 'h1', 'slug', 'seo_text')
            ->where('profession_id', $professionId)
            ->where('specialization_id', $specializationId)
            ->where('experience_id', $experienceId)
            ->where('english_level_id', $englishLevelId)
            ->where('technology_id', $technologyId)
            ->where('technology_two_id', $technologyTwoId)
            ->first();
        $metaData->seo_text = json_decode($metaData->seo_text);
        return $metaData ? $metaData->toArray() : null ;
    }

    public function getQueryParam($queryParams, $targetParam){
        if (array_key_exists($targetParam, $queryParams)){
            $result = explode(',', $queryParams[$targetParam])[0];
        } else {
            $result = null;
        }
        return $result;
    }

    public function getQueryParamTehnologies($queryParams, $targetParam){
        if (array_key_exists($targetParam, $queryParams)){
            $result = explode(',', $queryParams[$targetParam]);
        } else {
            $result = null;
        }
        return $result;
    }

    public function getMetaIntoSlug($slug){
        $filters = FiltersPage::query()
            ->where('slug', $slug)
            ->select('profession_id', 'specialization_id', 'experience_id', 'english_level_id', 'technology_id', 'technology_two_id')
            ->firstOrFail()
            ->toArray();

        $filterTitles = [
            'profession_id' => 'profession',
            'specialization_id' => 'specialization',
            'experience_id' => 'experience',
            'english_level_id' => 'englishLevel',
            'technology_id' => 'technology',
        ];

        $parameters = [];
        foreach ($filters as $filterKey => $filter){
            if (!is_null($filter)){
                if ($filterKey == 'technology_two_id'){
                    if (array_key_exists('technology' , $parameters)){
                        $parameters['technology'] =  $parameters['technology'] . ',' . $filter;
                    }
                } else {
                    $parameters[$filterTitles[$filterKey]]  = $filter;
                }
            }
        }
        return redirect()->route('specialists', $parameters);
    }

 public static function generate(Profession $professions, Experience $experience, Specialization $specializations, EnglishLevel $englishLevels, Technology $technology){
     $professions = $professions->fieldsForGenerator()->get()->concat([null]);
     $specializations = $specializations->fieldsForGenerator()->get()->concat([null]);
     $experiences = $experience->fieldsForGenerator()->get()->concat([null]);
     $englishLevels = $englishLevels->fieldsForGenerator()->get()->concat([null]);
     $technologies = $technology->fieldsForGenerator()->get()->concat([null]);

     $filterPage = new FiltersPage();

     foreach ($professions as $profession){
//         if (!in_array($profession->id??null, [2,9])){
//             continue;
//         }
         foreach ($specializations as $specialization) {
             if(!is_null($specialization) and !is_null($profession)){
                 if($specialization->profession_id !== $profession->id){
                     continue;
                 }
             }
             foreach ($englishLevels as $englishLevel){
                 foreach ($experiences as $experience){
                     foreach ($technologies as $technology){
                         if(!is_null($technology) and !is_null($profession)){
                             if(!($technology->professions()->where('profession_id', $profession->id)->exists()) ){
                                 continue;
                             }
                         }
                         if(!is_null($technology) and !is_null($specialization)){
                             if(!($technology->specializations()->where('specialization_id', $specialization->id)->exists())){
                                 continue;
                             }
                         }

                         foreach ($technologies as $technologyTwo){
                             if (is_null($technology) and !is_null($technologyTwo)){
                                 continue;
                             }
                             if ($technology === $technologyTwo and !is_null($technology)){
                                 continue;
                             }
                             if(!is_null($technologyTwo) and !is_null($profession)){
                                 if(!($technologyTwo->professions()->where('profession_id', $profession->id)->exists()) ){
                                     continue;
                                 }
                             }
                             if(!is_null($technologyTwo) and !is_null($specialization)){
                                 if(!($technologyTwo->specializations()->where('specialization_id', $specialization->id)->exists())){
                                     continue;
                                 }
                             }

                             $title = (!is_null($profession) ? "$profession->name, " : '') .
                                 (!is_null($experience) ? "$experience->name " : '') .
                                 (!is_null($specialization) ? "$specialization->name, " : '') .
                                 (!is_null($englishLevel) ? "English level $englishLevel->name, " : '') .
                                 (!is_null($technology) ? "$technology->name, " : '') .
                                 (!is_null($technologyTwo) ? "$technologyTwo->name, " : '');
                             $title = trim($title);
                             $title = rtrim($title, ',');

                             $randTitleKey = array_rand(self::$titleParts);
                             $randH1Key = array_rand(self::$h1Parts);
                             $randDescriptionKey = array_rand(self::$DescriptionParts);


                             $filterPage->create([
                                 'profession_id' => $profession->id ?? null,
                                 'specialization_id' => $specialization->id ?? null,
                                 'experience_id' => $experience->id ?? null,
                                 'english_level_id' => $englishLevel->id ?? null,
                                 'technology_id' => $technology->id ?? null,
                                 'technology_two_id' => $technologyTwo->id ?? null,
                                 'meta_title' => $title . ". " . self::$titleParts[$randTitleKey],
                                 'h1' => self::$h1Parts[$randH1Key] . ": " . $title,
                                 'meta_description' => str_replace('(генерация фильтров без добавленных фраз, только список фильтров)',$title , self::$DescriptionParts[$randDescriptionKey]),

                                 'slug' => Str::slug(
                                     (!is_null($profession) ? "$profession->name " : '') .
                                     (!is_null($experience) ? "$experience->name " : '') .
                                     (!is_null($specialization) ? "$specialization->name " : '') .
                                     (!is_null($englishLevel) ? "English level $englishLevel->name " : '') .
                                     (!is_null($technology) ? "$technology->name " : '') .
                                     (!is_null($technologyTwo) ? "$technologyTwo->name " : '')
                                     , '-'),
                             ]);
                         }
                     }
                 }
             }
         }
     }
 }

 public function makeWebpImages(){

        $extensions = [
            'png',
            'jpg',
        ];

        foreach(File::Files('storage/app/public/') as $file){

            $fileExtension = $file->getExtension();

            if (in_array($fileExtension, $extensions)){
                $fileWebp = Image::make($file)->stream('webp', 100);

                if(Storage::put($file->getFilenameWithoutExtension() . '.webp', $fileWebp, 'public')){
                    DB::table('nova_media_library')
                        ->where('name', $file->getFilename())
                        ->update([
                            'name' => $file->getFilenameWithoutExtension() . '.webp'
                        ]);
                    Storage::delete($file->getFilename());
                }
            }
        }
     return 'Script is done!';
 }

 public static function fillDbSeoTexts()
     {
        $profession = EnglishLevel::findOrFail(1);
        foreach (EnglishLevel::$management as $item)
        {
            $seoText = new SeoText(['content' => trim($item)]);
            $profession->seoTexts()->save($seoText);
        }
        return 'Script is done!';
     }

 public static function fillSeoTextFields($whatToDo)
     {
        $pages = FiltersPage::query()
            ->when($whatToDo == 'Record', function ($q){
                $q->whereNull('seo_text');
            })
            ->take(100000)
            ->get();

        foreach ($pages as $page)
        {
            $params = [
                'profession_id',
                "specialization_id",
                "experience_id",
                "technology_id",
                "technology_two_id",
                "english_level_id"
            ];
            $seoText = [];

            foreach ($params as $param)
            {
                if (isset($page->$param))
                {
                    if ($param === 'profession_id')
                    {
                        $res = Profession::query()->with('seoTexts', function ($q){
                            $q->inRandomOrder()
                                ->take(1);
                        })->findOrFail($page->$param);
                        $seoText[] = [
                            'item_title' => $res->getAttribute('title'),
                            'item_description'=> $res->seoTexts->firstOrFail()->content
                        ];
                    } else if($param === 'specialization_id')
                    {
                        $res = Specialization::query()->with('seoTexts', function ($q){
                            $q->inRandomOrder()
                                ->take(1);
                        })->findOrFail($page->$param);
                        $seoText[] = [
                            'item_title' => $res->getAttribute('title'),
                            'item_description'=> $res->seoTexts->firstOrFail()->content
                        ];
                    } else if($param === 'experience_id')
                    {
                        $res = Experience::query()->with('seoTexts', function ($q){
                            $q->inRandomOrder()
                                ->take(1);
                        })->findOrFail($page->$param);
                        $seoText[] = [
                            'item_title' => $res->getAttribute('title'),
                            'item_description'=> $res->seoTexts->firstOrFail()->content
                        ];
                    }else if($param === 'technology_id')
                    {
                        $res = Technology::query()
                            ->whereHas('seoTexts')
                            ->with('seoTexts', function ($q){
                            $q->inRandomOrder()
                                ->take(1);
                        })->find($page->$param);
                        if(isset($res)){
                            $seoText[] = [
                                'item_title' => $res->getAttribute('title'),
                                'item_description'=> $res->seoTexts->firstOrFail()->content
                            ];
                        } else {
                            continue;
                        }
                    } else if($param === 'technology_two_id')
                    {
                        $res = Technology::query()
                            ->whereHas('seoTexts')
                            ->with('seoTexts', function ($q){
                            $q->inRandomOrder()
                                ->take(1);
                        })->find($page->$param);
                        if(isset($res)){
                            $seoText[] = [
                                'item_title' => $res->getAttribute('title'),
                                'item_description'=> $res->seoTexts->firstOrFail()->content
                            ];
                        } else {
                            continue;
                        }

                    } else if($param === 'english_level_id')
                    {
                        $res = EnglishLevel::query()->with('seoTexts', function ($q){
                            $q->inRandomOrder()
                                ->take(1);
                        })->findOrFail($page->$param);
                        $seoText[] = [
                            'item_title' => $res->getAttribute('title'),
                            'item_description'=> $res->seoTexts->firstOrFail()->content
                        ];
                    }
                }
            }
            $page->seo_text = $seoText;
            $page->save();
        }
     }
}
