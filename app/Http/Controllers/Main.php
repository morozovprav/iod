<?php

namespace App\Http\Controllers;

use App\Models\Experience;
use App\Models\FiltersPage;
use \App\Models\Profession;
use App\Models\Team;
use App\Models\Technology;
use App\Models\MainSpecialist;
use App\Models\Specialist;

class Main extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {

        $data = \App\Models\Main::query()
            ->with('specialists', function ($q){
                $q->select('name', 'main_img', 'slug', 'profession_id', 'specialists.id', 'status_id', 'english_level_id', 'english_is_checked')
                    ->display()
                    ->with('profession', function ($q){
                        $q->select('title', 'professions.id');
                  })
                    ->with('specializations', function ($q){
                        $q->select('title', 'specializations.id');
                    })
                    ->with('status')
                    ->with('englishLevel')
                    ->withCount('teams')
                    ->limit(6);

        })
        ->firstOrFail();

        foreach ($data->specialists as $specKey => $specialist){

            foreach ($specialist->specializations as $key => $specialization){
                $exp = Experience::query()
                    ->select('title', 'info')
                    ->findOrFail($specialization->pivot->experience_id);
                $exp = $exp->getFullData();
                $data['specialists'][$specKey]['specializations'][$key]->experience = $exp;
            }
        }
        $data = $data->getFullData();

//            foreach ($data['specialists'] as $specialist){
//                $specialist['status'] = $specialist['status'][0]['title'];
//                $specData[] = $specialist;
//            }
//            $data['specialists'] = $specData;


        $professions = Profession::query()
            ->with('technologies', function ($q){
                $q->select('title', 'technologies.id')
//                ->orderBy('sort_order')
                ->whereHas('specialists');
            })
            ->with('specialists', function ($q){
                $q->select('name',
                    'main_img',
                    'slug',
                    'profession_id',
                    'id',
                    'status_id',
                    'english_level_id',
                    'english_is_checked')

                    ->with('profession', function ($q){
                        $q->select('title', 'professions.id');
                    })
                    ->with('specializations', function ($q){
                        $q->select('title', 'specializations.id');
                    })
                    ->getSpecializationWithExperience()
                    ->display()
                    ->with('status', 'englishLevel', 'technologies:id,title')
                    ->withCount('teams');
            })
            ->get();

//        foreach($professions as $key => $profession){
//            $r = $professions[$key]->specialists
//                ->sortByDesc(function ($q){
//                if (isset($q->specializations[0])){
//                    return $q->specializations[0]->exp_sort_order;
//                } else {
//                    return 1;
//                }
//            });
//            $professions[$key]->specialists->sortBy('name');
//            unset($professions[$key]->specialists);
//            $professions[$key]->specialists = $r;
//        }

//dd($professions[0]->specialists);
        $slug = FiltersPage::query()
            ->select('slug', 'id', 'technology_id', 'profession_id')
            ->whereNotNull('technology_id')
            ->whereNotNull('profession_id')
            ->whereNull(['english_level_id', 'specialization_id', 'experience_id', 'technology_two_id'])
            ->get();

        foreach ($professions as $profession){
            foreach ($profession->specialists as $specKey => $specialist){
                foreach ($specialist->specializations as $key => $specialization){
                    $exp = Experience::query()
                        ->select('title', 'info')
                        ->findOrFail($specialization->pivot->experience_id);
                    $exp = $exp->getFullData();
                    $profession['specialists'][$specKey]['specializations'][$key]->experience = $exp;
                }
            }

            foreach ($profession->technologies as $technoKey => $technology){
                $findedSlug = $slug->where('technology_id', $technology->id)->where('profession_id', $profession->id)->first();

                $profession->technologies[$technoKey]['slug'] = $findedSlug ? $findedSlug->slug : null;
            }
            $catalogData[] = $profession->getFullData();
        }

        foreach($catalogData as $key => $profession){
            $r = collect($catalogData[$key]['specialists'])

                ->sortBy(function ($q){;
                    if (isset($q['specializations'][0])){
                        return $q['specializations'][0]['exp_sort_order'];
                    } else {
                        return 1;
                    }
                })->slice(0,5)->values()->toArray();
            unset($catalogData[$key]['specialists']);
            $catalogData[$key]['specialists'] = $r;
        }

//        foreach ($catalogData as $key => $profession){
//            if (array_key_exists('specialists', $profession)){
//                $specData = [];
//                foreach ($profession['specialists'] as $specialist){
//                    $specialist['status'] = $specialist['status'][0]['title'];
//                    $specData[] = $specialist;
//                }
//                $profession['specialists'] = $specData;
//                $catalogData[$key] = $profession;
//            }
//
//        }

        $teams = Team::query()
                ->addManualSort()
                ->with('specialists', function ($q){
                    $q->select('name', 'main_img', 'slug', 'specialists.id', 'status_id', 'profession_id', 'english_level_id', 'english_is_checked')
                        ->display()
                        ->with('profession', 'status', 'specializations', 'englishLevel', 'technologies:id,title')
                        ->withCount('teams');
                })->get();

        foreach ($teams as $profession){
            foreach ($profession->specialists as $specKey => $specialist){

                foreach ($specialist->specializations as $key => $specialization){
                    $exp = Experience::query()
                        ->select('title', 'info')
                        ->findOrFail($specialization->pivot->experience_id);
                    $exp = $exp->getFullData();
                    $profession['specialists'][$specKey]['specializations'][$key]->experience = $exp;
                }
            }
            $teamsData[] = $profession->getFullData();
        }

//        $teams = $catalogData;
//        foreach ($teams as $key => $team){
//            $teamsData[$key] = $team->getFullData();
//            if (array_key_exists('specialists', $teamsData[$key])){
//                foreach ($teamsData[$key]['specialists'] as $teamsDatum){
//
//                    $teamsDatum['status'] = $teamsDatum['status'][0]['title'];
//                    $spec[] = $teamsDatum;
//                }
//                $teamsData[$key]['specialists'] = $spec;
//            }
//
//        }




        return response()->json([
            'status' => 'success',
            'content' => $data,
            'block_catalog' => $catalogData,
            'teams' => $teamsData
        ]);
    }
}
