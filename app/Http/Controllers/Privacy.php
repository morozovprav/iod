<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Privacy extends Controller
{
    public function index(){
        $data =\App\Models\Privacy::firstOrFail();
        $content = $data->getFullData();

        return response()->json([
            'status' => 'success',
            'content' => $content,
        ]);
    }
}
