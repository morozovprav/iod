<?php

namespace App\Http\Controllers;

use App\Models\About;
use App\Models\TeamsPage;
use Illuminate\Http\Request;

class SpecialistPage extends Controller
{
    public function index()
    {
        $data = \App\Models\SpecialistPage::firstOrFail();
        $content = $data->getFullData();

        $teams = TeamsPage::query()->select(
            'team_block_composition_label',
            'team_block_description_label',
            'team_block_button_title',
            'team_block_members_label',
            'popup_title',
            'popup_description',
            'popup_btn_title')
            ->firstOrFail();
        $teams = $teams->getFullData();

        return response()->json([
            'status' => 'success',
            'content' => $content,
            'teams_block' => $teams,
        ]);
    }
}
