<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Page404 extends Controller
{
    public function index(){
        $data = \App\Models\Page404::firstOrFail();
        $data = $data->getFullData();

        return response()->json([
            'status' => 'success',
            'content' => $data
        ]);
    }
}
