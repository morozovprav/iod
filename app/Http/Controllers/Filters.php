<?php

namespace App\Http\Controllers;

use App\Filters\SpecialistFilter;
use App\Models\EnglishLevel;
use App\Models\Experience;
use App\Models\Profession;
use App\Models\Specialization;
use App\Models\Status;
use App\Models\Technology;
use Illuminate\Http\Request;

class Filters extends Controller
{
    public function getFilters(SpecialistFilter $filters){

        $specialistsId = \App\Models\Specialist::filter($filters)
            ->display()
            ->get('id')
            ->pluck('id')
            ->toArray();

        if (count($specialistsId) == 0) {
            $specialistsId = \App\Models\Specialist::query()
                ->display()
                ->get('id')
                ->pluck('id')
                ->toArray();
        }


        $englishLevel = EnglishLevel::query()
            ->whereHas('specialists', function ($q) use($specialistsId){
                $q->whereIn('specialists.id', $specialistsId);
            })->get(['id', 'title']);

        $professions = Profession::query()
            ->whereHas('specialists', function ($q) use($specialistsId){
                $q->whereIn('specialists.id', $specialistsId);
            })->get(['id', 'title']);
        $prof= [];
        foreach ($professions as $profession){
            $prof[] = $profession->getFullData();
        }

        $specializations = Specialization::query()
            ->whereHas('specialistsForFilters', function ($q) use($specialistsId){
                $q->whereIn('specialists.id', $specialistsId);
            })->get(['id', 'title']);
        $spec = [];
        foreach ($specializations as $specialization){
            $spec[] = $specialization->getFullData();
        }

        $experience = Experience::query()
            ->whereHas('specialistSpecializations', function ($q) use($specialistsId){
                $q->whereIn('specialist_id', $specialistsId);
            })->orderBy('sort_order')->get(['id', 'title']);

        $technology = Technology::query()
            ->whereHas('specialists', function ($q) use($specialistsId){
                $q->whereIn('specialists.id', $specialistsId);
            })
//            ->orderBy('sort_order')
            ->get(['id', 'title']);

        $statuses = Status::query()
            ->whereHas('specialists', function ($q) use($specialistsId){
                $q->whereIn('specialists.id', $specialistsId);
            })
            ->get(['id', 'title']);
        $stat = [];
        foreach ($statuses as $status){
            $stat[] = $status->getFullData();
        }

        return [
            'profession' => $prof,
            'specialization' => $spec,
            'experience' => $experience,
            'technology' => $technology,
            'englishLevel' => $englishLevel,
            'status' => $stat,
        ];
    }
}
