<?php

namespace App\Http\Controllers;

use App\Filters\SpecialistFilter;
use App\Models\Experience;
use App\Models\FiltersPage;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\App;
use function App\Nova\Flexible\Resolvers\get;

class Specialist extends Controller
{
    public function getSpecialists(SpecialistFilter $filters, Request $request)
    {

        $specialists = \App\Models\Specialist::filter($filters)
            ->display()
            ->select('name', 'main_img', 'profession_id', 'status_id', 'slug', 'id', 'english_level_id')
            ->with('status', 'englishLevel', 'technologies:id,title')
            ->GetSpecializationWithExperience()
            ->with('profession', function ($q){
                $q->select('title', 'id');
            })
            ->withCount('teams')
            ->get();
        $r = $specialists->sortBy(function ($q){
                if (isset($q->specializations[0])){
                    return $q->specializations[0]->exp_sort_order;
                } else {
                    return 1;
                }
            })->values();

        $perPage = 12;
        if (request("page")) {
            $currentPage = request("page") * $perPage - $perPage;
        } else {
            $currentPage = 0;
        }

        $specialists = new LengthAwarePaginator(
            $r->slice($currentPage, $perPage),
            $r->count(),
            $perPage,
            $currentPage,
            [
                'path' => request()->url(),
                'query' => request()->query(),
            ]
        );

       $availableFilters = (new Filters)->getFilters($filters);
       $metaData = (new SeoController)->getMeta($request);

        $paginationData = [
            'all_pages' => $specialists->lastPage(),
            'current_page' => $specialists->currentPage(),
            'per_page' => $specialists->perPage(),
            'total' => $specialists->total(),
            'get_param' => $specialists->getOptions(),
            'next_page' => $specialists->nextPageUrl()
        ];

        if (count($specialists) > 0){
            foreach ($specialists as $specialist){
                foreach ($specialist->specializations as $key => $specialization){
                    $exp = Experience::query()
                        ->select('title', 'info', 'id')
                        ->findOrFail($specialization->pivot->experience_id);
                    $exp = $exp->getFullData();
                    $specialist['specializations'][$key]->experience = $exp;
                }
                $specialistData[] = $specialist->getFullData();
            }
        } else {
            $specialistData = [];
        }

        return response()->json([
        'status' => 'success',
        'result' => $specialistData,
        'paginate_data' => $paginationData,
        'available_filters' => $availableFilters,
        'meta_data' => $metaData,
    ]);
    }


    public function getOneSpecialist(Request $request)
    {
        $data =\App\Models\Specialist::query()
            ->display()
            ->where('slug', $request->slug)
            ->with('technologies', 'status', 'englishLevel')
            ->GetSpecializationWithExperience()
            ->with('teams', function ($q){
                $q->with('specialists', function ($q){
                    $q->select('name', 'main_img', 'slug', 'status_id', 'profession_id', 'english_level_id', 'specialists.id')
                        ->display()
                        ->with('profession', 'status', 'englishLevel', 'technologies')
                        ->GetSpecializationWithExperience();
                })
                ->limit(3);
            })
            ->with('profession', function ($q){
                $q->select('title', 'id');
            })
            ->withCount('teams')
            ->firstOrFail();
//        dd($data);
//TODO Переделать архитектуру конструкции ниже!
        foreach ($data->teams as $teamKey => $team){
            foreach ($team->specialists as $specKey => $specialist){
                    foreach ($specialist->specializations as $key => $specialization){
                        $exp = Experience::query()
                            ->select('title', 'info', 'id')
                            ->findOrFail($specialization->pivot->experience_id);
                        $exp = $exp->getFullData();
                        $specialist['specializations'][$key]->experience = $exp;
                    }
                $data->teams[$teamKey]->specialists[$specKey]->specializations[$key]->experience = $exp;
            }
        }

        foreach ($data->specializations as $key => $specialization){
            $exp = Experience::query()
                ->select('title', 'info', 'id')
                ->findOrFail($specialization->pivot->experience_id);
            $exp = $exp->getFullData();
            $data['specializations'][$key]->experience = $exp;
        }

        $slug = FiltersPage::query()
            ->select('slug', 'id', 'technology_id', 'profession_id')
            ->whereNotNull('technology_id')
            ->whereNotNull('profession_id')
            ->whereNull(['english_level_id', 'specialization_id', 'experience_id', 'technology_two_id'])
            ->get();

        foreach ($data->technologies as $technoKey => $technology){
            $findedSlug = $slug->where('technology_id', $technology->id)->where('profession_id', $data->profession->id)->first();
            $data->technologies[$technoKey]['slug'] = $findedSlug ? $findedSlug->slug : null;
        }

        $englishLevel = FiltersPage::query()
            ->select('slug', 'id', 'english_level_id',)
            ->whereNotNull('english_level_id')
            ->whereNull(['profession_id', 'technology_id', 'specialization_id', 'experience_id', 'technology_two_id'])
            ->get();

        $findedSlug = $englishLevel->where('english_level_id', $data->englishLevel->id)->first();
        $data->englishLevel['slug'] = $findedSlug ? $findedSlug->slug : null;

        $specializations = FiltersPage::query()
            ->select('slug', 'id', 'specialization_id', 'profession_id')
            ->whereNotNull('specialization_id')
            ->whereNotNull('profession_id')
            ->whereNull(['english_level_id', 'experience_id', 'technology_two_id', 'technology_id'])
            ->get();

        $experience = FiltersPage::query()
            ->select('slug', 'id', 'experience_id')
            ->whereNotNull('experience_id')
            ->whereNull(['english_level_id', 'specialization_id', 'technology_two_id', 'technology_id', 'profession_id'])
            ->get();

        foreach ($data->specializations as $specKey => $specialization){
            $findedSlug = $specializations->where('specialization_id', $specialization->id)->where('profession_id', $data->profession->id)->first();
            $data->specializations[$specKey]['slug'] = $findedSlug ? $findedSlug->slug : null;

            $expSlug = $experience->where('experience_id', $specialization->experience['id'])->first();
            $expResultData = $data->specializations[$specKey]->experience;
            $expResultData['slug'] = $expSlug ? $expSlug->slug : null;
            $data->specializations[$specKey]->experience = $expResultData;
        }

        $content = $data->getFullData();
//        if (array_key_exists('teams', $content)){
//            foreach ($content['teams'] as $key => $team){
//                foreach ($team['specialists'] as $specialist){
//                    $specialist['status'] = $specialist['status'][0]['title'];
//                    $specData[] = $specialist;
//                }
//                $team['specialists'] = $specData;
//                $content['teams'][$key] = $team;
//            }
//        }


        $collegues = \App\Models\Specialist::query()
            ->select('name', 'main_img', 'profession_id', 'status_id', 'slug', 'id', 'english_level_id')
            ->display()
            ->whereHas('profession', function ($q) use($data) {
                $q->where('id', $data['profession_id']);
            })
            ->where('id', '!=', $data['id'])

            ->with('status', 'englishLevel', 'technologies')
            ->getSpecializationWithExperience()
            ->with('profession', function ($q){
                $q->select('title', 'id');
            })
            ->withCount('teams')
            ->get()
            ->sortByDesc(function ($q){
                if (isset($q->specializations[0])){
                    return $q->specializations[0]->exp_sort_order;
                } else {
                    return 1;
                }
            });

        if (count($collegues) > 0){
            foreach ($collegues as $specialist){
                foreach ($specialist->specializations as $key => $specialization){
                    $exp = Experience::query()
                        ->select('title', 'info', 'id')
                        ->findOrFail($specialization->pivot->experience_id);
                    $exp = $exp->getFullData();
                    $specialist['specializations'][$key]->experience = $exp;
                }
                $colleguesData[] = $specialist->getFullData();
            }
        } else {
            $colleguesData = [];
        }

        return response()->json([
            'status' => 'success',
            'content' => $content,
            'collegues' => $colleguesData,
        ]);
    }


}
