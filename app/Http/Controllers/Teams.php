<?php

namespace App\Http\Controllers;

use App\Models\Experience;
use App\Models\Team;
use App\Models\TeamsPage;
use Illuminate\Http\Request;

class Teams extends Controller
{
    public function getTeamsPage()
    {
// Получаем контент страницы команд
        $data = TeamsPage::firstOrFail();
        $content = $data->getFullData();

//Получаем контент одной команды для блока команд
        $oneTeam = $this->getOneTeamForTeamsPage();

//Получаем названия и слаги для табов блока команд
        $teams_title = $this->getTeamsTab();

//        $form = \App\Models\Contact::query()
//            ->select('form_name_placeholder',
//                'form_email_placeholder',
//                'form_phone_placeholder',
//                'form_textarea_placeholder',
//                'form_privacy_first_part_label',
//                'form_privacy_second_part_label',
//                'form_file_label',
//                'form_button_title',
//                'email',
//                'phone',
//                'messengers',
//            )
//            ->firstOrFail();
//        $form = $form->getFullData();

        return response()->json([
            'status' => 'success',
            'content' => $content,
            'one_team' => $oneTeam,
            'teams_tab' => $teams_title,
//            'form' => $form,
        ]);
    }

    public function getOneTeam(Request $request){
        $oneTeam = Team::query()
            ->addManualSort()
            ->with('specialists', function ($q){
                $q->select('name', 'main_img', 'slug', 'status_id', 'profession_id', 'specialists.id', 'english_level_id')
                    ->with('status', 'specializations', 'englishLevel', 'technologies:id,title')
                    ->with('profession', function ($q){
                        $q->select('title', 'id');
                    })
                    ->withCount('teams');
            })
            ->where('slug', $request->slug)
            ->firstOrFail();

        if (count($oneTeam['specialists']) > 0){
            foreach ($oneTeam['specialists'] as $specialist){
                foreach ($specialist->specializations as $key => $specialization){
                    $exp = Experience::query()
                        ->select('title', 'info', 'id')
                        ->findOrFail($specialization->pivot->experience_id);
                    $exp = $exp->getFullData();
                    $specialist['specializations'][$key]->experience = $exp;
                }
                $specialistData[] = $specialist->getFullData();
            }
            $oneTeam['specialists'] = $specialistData;
        } else {
            $specialistData = [];
        }

        $oneTeam = $oneTeam->getFullData();

        return response()->json([
            'status' => 'success',
            'content' => $oneTeam,
        ]);
    }

    public function getTeamsTab(){
        $teamsTab = Team::query()
            ->addManualSort()
            ->select('title_task', 'slug', 'short_title')
            ->get();
        foreach ($teamsTab as $item){
            $teams_title[] = $item->getFullData();
        }
        return $teams_title;
    }

    public function getOneTeamForTeamsPage(){
        $oneTeam = Team::query()
            ->addManualSort()
            ->with('specialists', function ($q){
                $q->select('name', 'main_img', 'profession_id', 'status_id', 'slug', 'specialists.id', 'english_level_id')
                    ->with('status', 'specializations', 'englishLevel', 'technologies:id,title')
                    ->with('profession', function ($q){
                        $q->select('title', 'id');
                    })
                    ->withCount('teams');
            })
            ->firstOrFail();

        if (count($oneTeam['specialists']) > 0){
            foreach ($oneTeam['specialists'] as $specialist){
                foreach ($specialist->specializations as $key => $specialization){
                    $exp = Experience::query()
                        ->select('title', 'info', 'id')
                        ->findOrFail($specialization->pivot->experience_id);
                    $exp = $exp->getFullData();
                    $specialist['specializations'][$key]->experience = $exp;
                }
                $specialistData[] = $specialist->getFullData();
            }
            $oneTeam['specialists'] = $specialistData;
        } else {
            $specialistData = [];
        }

        $oneTeam = $oneTeam->getFullData();
//        foreach ($oneTeam['specialists'] as $specialist){
//            $specialist['status'] = $specialist['status'][0]['title'];
//            $result[] = $specialist;
//        }
//        $oneTeam['specialists'] = $result;

        return $oneTeam;
    }
}
