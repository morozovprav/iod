<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class requestPopupsPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'max:255',
            'phone' => 'max:25',
             'mail' => 'required','email:rfc,dns',
            'message' => 'required|max:2000',
           'file' => 'mimes:txt,doc,docx,pdf,xls,xlsx',
        ];
    }
}
