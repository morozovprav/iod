<?php

namespace App\Console\Commands;

use App\Http\Controllers\SeoController;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;

class GeneratePages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        app()->call([SeoController::class, 'generate']);
    }
}
