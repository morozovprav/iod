<?php

namespace App\Console\Commands;

use App\Http\Controllers\SeoController;
use Illuminate\Console\Command;

class FillSeoTextFields extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seo:fill-seo-texts-in-filters-page';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $whatToDo = $this->choice('What do you wanna do?', ['Rerecord', 'Record']);
        app()->call([SeoController::class, 'fillSeoTextFields'], ['whatToDo'=> $whatToDo]);
    }
}
