<?php

namespace App\Console\Commands;

use App\Http\Controllers\SeoController;
use Illuminate\Console\Command;

class ReloadImageToWebp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seo:image';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change images extension to webp';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $result = app(SeoController::class)->makeWebpImages();
        $this->info($result);
    }
}
