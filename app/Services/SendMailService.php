<?php


namespace App\Services;



use App\Http\Requests\requestPopupsPost;
use Illuminate\Support\Facades\Mail;
use App\Models\EmailSetting;
use function Symfony\Component\String\b;

class SendMailService
{

    private static function config()
    {
        $emailSetting = EmailSetting::firstOrFail();
        config([
            "mail.mailers.smtp.username" => $emailSetting->email_for_send,
            "mail.mailers.smtp.password" => $emailSetting->password,
        ]);

        return $emailSetting;
    }



    public static function sendEmailToAdmin($popup, $postData, requestPopupsPost $request = null)
    {

        $emailSetting = SendMailService::config();

        switch ($popup) {
            case 'request':
                $mail = $emailSetting->email_for_request;
                $postData['clientMessage'] = $postData['message'];
                $view = 'toAdminFromRequest';
                break;
        }
        Mail::send('email.' . $view, $postData, function ($message) use ($emailSetting, $popup, $mail, $request, $postData) {
            $message->to($mail, $emailSetting->name)->subject($emailSetting->name . ': New mail from the ' . $popup . ' popup!');
            $message->from($emailSetting->email_for_send, $emailSetting->name);
        });
       // dd($emailSetting);
    }
}
