<?php

namespace App\Filters;

class SpecialistFilter extends QueryFilter
{
    public function status($id)
    {
        return $this->builder->whereHas('status', function ($q) use ($id){
            $q->where('id', $id);
        });
    }

    public function englishLevel($levelIds)
    {
        return $this->builder->whereHas('englishLevel', function ($q) use ($levelIds){
            $q->whereIn('id', $this->paramToArray($levelIds));
        });
    }

    public function profession($id)
    {
        return $this->builder->whereHas('profession', function ($q) use ($id){
            $q->where('id', $id);
        });
    }

    public function technology($tehnologyIds)
    {
        return $this->builder->whereHas('technologies', function ($query) use ($tehnologyIds){
            $query->distinct()->whereIn('technologies.id', $this->paramToArray($tehnologyIds));
        }, '=', count($this->paramToArray($tehnologyIds)));
    }

    public function specialization($specializationIds)
    {
        return $this->builder->whereHas('specializations', function ($query) use ($specializationIds){
            $query->distinct()->whereIn('specializations.id', $this->paramToArray($specializationIds));
        });
    }

    public function experience($experienceIds, $specializationIds = null)
    {
        if (!empty($specializationIds)){
            return $this->builder->whereHas('specializations', function ($query) use ($experienceIds, $specializationIds){
                $query->distinct()->whereIn('specialist_specialization.experience_id', $this->paramToArray($experienceIds));
                $query->distinct()->whereIn('specialist_specialization.specialization_id', $this->paramToArray($specializationIds));
            });
        } else {
            return $this->builder->whereHas('specializations', function ($query) use ($experienceIds, $specializationIds){
                $query->distinct()->whereIn('specialist_specialization.experience_id', $this->paramToArray($experienceIds));
            });
        }
    }
}
