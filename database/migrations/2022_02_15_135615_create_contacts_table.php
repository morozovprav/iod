<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->json('h1_title')->nullable();
            $table->json('work_hours_label')->nullable();
            $table->json('work_hours')->nullable();
            $table->json('work_hours_time_zone_city')->nullable();
            $table->json('office_address_label')->nullable();
            $table->json('office_address')->nullable();
            $table->json('email_label')->nullable();
            $table->json('email')->nullable();
            $table->json('phone_label')->nullable();
            $table->json('phone')->nullable();
            $table->json('social_label')->nullable();
            $table->json('social')->nullable();
            $table->json('form_name_placeholder')->nullable();
            $table->json('form_email_placeholder')->nullable();
            $table->json('form_phone_placeholder')->nullable();
            $table->json('form_textarea_placeholder')->nullable();
            $table->json('form_privacy_label')->nullable();
            $table->json('form_button_title')->nullable();
            $table->json('meta_title')->nullable();
            $table->json('meta_description')->nullable();
            $table->json('meta_keyword')->nullable();
            $table->json('og_title')->nullable();
            $table->json('og_description')->nullable();
            $table->json('og_img')->nullable();
            $table->json('messengers_label')->nullable();
            $table->json('messengers')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
