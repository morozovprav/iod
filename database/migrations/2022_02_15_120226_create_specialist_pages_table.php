<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecialistPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specialist_pages', function (Blueprint $table) {
            $table->id();
            $table->json('specialization_label')->nullable();
            $table->json('work_experience_label')->nullable();;
            $table->json('english_level_label')->nullable();;
            $table->json('technoligy_label')->nullable();;
            $table->json('project_label')->nullable();;
            $table->json('certificate_label')->nullable();;
            $table->json('courses_label')->nullable();;
            $table->json('teams_label')->nullable();;
            $table->json('other_collegues_label')->nullable();;
            $table->json('spec_buton_title')->nullable();;
            $table->json('teams_buton_title')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specialist_pages');
    }
}
