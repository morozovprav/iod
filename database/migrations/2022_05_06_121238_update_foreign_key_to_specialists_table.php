<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateForeignKeyToSpecialistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('specialists', function (Blueprint $table) {
            $table->dropForeign('specialists_profession_id_foreign');
            $table->dropForeign('specialists_english_level_id_foreign');
            $table->dropForeign('specialists_status_id_foreign');
            $table->foreign('profession_id')->references('id')->on('professions')->onDelete('set null');
            $table->foreign('english_level_id')->references('id')->on('english_levels')->onDelete('set null');
            $table->foreign('status_id')->references('id')->on('statuses')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('specialists', function (Blueprint $table) {
            $table->dropForeign('specialists_profession_id_foreign');
            $table->dropForeign('specialists_english_level_id_foreign');
            $table->dropForeign('specialists_status_id_foreign');
            $table->foreign('profession_id')->references('id')->on('professions')->onDelete('cascade');
            $table->foreign('english_level_id')->references('id')->on('english_levels')->onDelete('cascade');
            $table->foreign('status_id')->references('id')->on('statuses')->onDelete('cascade');
        });
    }
}
