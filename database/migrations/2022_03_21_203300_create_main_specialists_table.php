<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainSpecialistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_specialists', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('main_id')->unsigned();
            $table->foreign('main_id')->references('id')->on('mains')->onDelete('cascade');
            $table->bigInteger('specialist_id')->unsigned();
            $table->foreign('specialist_id')->references('id')->on('specialists')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('main_specialists', function (Blueprint $table) {
            $table->dropForeign(['main_id']);
            $table->dropForeign(['specialist_id']);

        });
        Schema::dropIfExists('main_specialists');
    }
}
