<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPrivacyLabelPartToContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->renameColumn('form_privacy_label', 'form_privacy_first_part_label');
            $table->json('form_privacy_second_part_label')
                ->nullable()
                ->after('form_privacy_label');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->renameColumn('form_privacy_first_part_label', 'form_privacy_label');
            $table->dropColumn('form_privacy_second_part_label');
        });
    }
}
