<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mains', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('Description')->nullable();
            $table->string('h1_text')->nullable();
            $table->string('h2_text')->nullable();
            $table->bigInteger('specialist_id')->unsigned()->nullable();
            $table->foreign('specialist_id')->references('id')->on('specialists');
            $table->string('see_all_btn_txt')->nullable();
            $table->string('see_all_btn_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mains', function (Blueprint $table) {
            $table->dropForeign(['specialist_id']);
            $table->dropColumn('specialist_id');
        });
        Schema::dropIfExists('mains');
    }
}
