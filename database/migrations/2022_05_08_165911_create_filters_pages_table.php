<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFiltersPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filters_pages', function (Blueprint $table) {
            $table->id();
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keyword')->nullable();
            $table->string('h1')->nullable();
            $table->string('slug')->unique()->nullable();
            $table->bigInteger('profession_id')->unsigned()->nullable();
            $table->foreign('profession_id')->references('id')->on('professions')->onDelete('set null');
            $table->bigInteger('specialization_id')->unsigned()->nullable();
            $table->foreign('specialization_id')->references('id')->on('specializations')->onDelete('set null');
            $table->bigInteger('experience_id')->unsigned()->nullable();
            $table->foreign('experience_id')->references('id')->on('experiences')->onDelete('set null');
            $table->bigInteger('english_level_id')->unsigned()->nullable();
            $table->foreign('english_level_id')->references('id')->on('english_levels')->onDelete('set null');
            $table->bigInteger('technology_id')->unsigned()->nullable();
            $table->foreign('technology_id')->references('id')->on('technologies')->onDelete('set null');
            $table->bigInteger('technology_two_id')->unsigned()->nullable();
            $table->foreign('technology_two_id')->references('id')->on('technologies')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filters_pages');
    }
}
