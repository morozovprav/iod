<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abouts', function (Blueprint $table) {
            $table->id();
            $table->json('hero_h1_title')->nullable();
            $table->json('hero_img')->nullable();
            $table->json('hero_description_left')->nullable();
            $table->json('hero_description_botom')->nullable();
            $table->json('advantage')->nullable();
            $table->json('numeric_list')->nullable();
            $table->json('mark_list')->nullable();
            $table->json('paragraphs')->nullable();
            $table->json('galery')->nullable();
            $table->json('advantage_botom')->nullable();
            $table->json('meta_title')->nullable();
            $table->json('meta_description')->nullable();
            $table->json('meta_keyword')->nullable();
            $table->json('og_title')->nullable();
            $table->json('og_description')->nullable();
            $table->json('og_img')->nullable();
            $table->json('paragraph')->nullable();
            $table->json('item_title')->nullable();
            $table->json('item_description')->nullable();
            $table->json('img')->nullable();
            $table->json('img_title')->nullable();
            $table->json('img_alt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abouts');
    }
}
