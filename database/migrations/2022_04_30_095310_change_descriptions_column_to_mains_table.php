<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDescriptionsColumnToMainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mains', function (Blueprint $table) {
            $table->json('title')->change();
            $table->json('Description')->change();
            $table->json('h1_text')->change();
            $table->json('h2_text')->change();
            $table->json('see_all_btn_txt')->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mains', function (Blueprint $table) {
            $table->string('title')->change();
            $table->string('Description')->change();
            $table->string('h1_text')->change();
            $table->string('h2_text')->change();
            $table->string('see_all_btn_txt')->change();
        });
    }
}
