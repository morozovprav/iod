<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams_pages', function (Blueprint $table) {
            $table->id();
            $table->json('meta_title')->nullable();
            $table->json('meta_description')->nullable();
            $table->json('meta_keyword')->nullable();
            $table->json('og_title')->nullable();
            $table->json('og_description')->nullable();
            $table->string('og_img')->nullable();

            $table->json('hero_h1_title')->nullable();
            $table->json('hero_paragraph')->nullable();
            $table->string('hero_img')->nullable();
            $table->string('hero_img_title')->nullable();
            $table->string('hero_img_alt')->nullable();
            $table->json('why_hire_block_title')->nullable();
            $table->json('why_hire_left_paragraphs')->nullable();
            $table->json('why_hire_mark_list')->nullable();
            $table->json('why_hire_right_paragraph')->nullable();
            $table->string('why_hire_img')->nullable();
            $table->string('why_hire_img_title')->nullable();
            $table->string('why_hire_img_alt')->nullable();
            $table->json('consolidate_title')->nullable();
            $table->string('consolidate_img')->nullable();
            $table->string('consolidate_img_title')->nullable();
            $table->string('consolidate_img_alt')->nullable();
            $table->json('consolidate_right_paragraphs')->nullable();
            $table->json('consolidate_mark_list')->nullable();
            $table->json('advantage_block_title')->nullable();
            $table->json('advantage')->nullable();
            $table->json('how_we_work_block_title')->nullable();
            $table->string('how_we_work_img')->nullable();
            $table->string('how_we_work_img_title')->nullable();
            $table->string('how_we_work_img_alt')->nullable();
            $table->json('how_we_work_left_paragraph')->nullable();
            $table->json('how_we_work_right_paragraphs')->nullable();
            $table->json('hire_a_team_for_your_task_title')->nullable();
            $table->json('hire_a_team_for_your_task_description')->nullable();
            $table->json('form_title')->nullable();
            $table->json('form_description')->nullable();
            $table->json('team_block_composition_label')->nullable();
            $table->json('team_block_description_label')->nullable();
            $table->json('team_block_button_title')->nullable();
            $table->json('team_block_members_label')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams_pages');
    }
}
