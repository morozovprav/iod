<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mains', function (Blueprint $table) {
            $table->json('meta_title')->nullable();
            $table->json('meta_description')->nullable();
            $table->json('meta_keyword')->nullable();
            $table->json('og_title')->nullable();
            $table->json('og_description')->nullable();
            $table->string('og_img')->nullable();
            $table->json('hero_description');
            $table->json('hero_h1_title');
            $table->json('hero_button_title')->nullable();
            $table->json('our_services_title');
            $table->json('our_services');
            $table->json('our_services_btn_title');
            $table->json('about_as_title');
            $table->json('about_us_paragraphs');
            $table->json('about_us_you_can_save_prefix');
            $table->json('about_us_you_can_save_percent');
            $table->json('about_us_you_can_save_text');
            $table->string('about_as_img');
            $table->string('about_as_img_title')->nullable();
            $table->string('about_as_img_alt')->nullable();
            $table->string('white_logo');
            $table->json('running_text');
            $table->json('outstaffing_benefits_title');
            $table->json('outstaffing_benefits_items');
            $table->json('hire_a_team_title');
            $table->json('hire_a_team_description');
            $table->json('hire_a_team_btn_title')->nullable();
            $table->json('partners');
            $table->json('form_title');
            $table->json('form_description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mains', function (Blueprint $table) {
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_description');
            $table->dropColumn('meta_keyword');
            $table->dropColumn('og_title');
            $table->dropColumn('og_description');
            $table->dropColumn('og_img');
            $table->dropColumn('hero_description');
            $table->dropColumn('hero_h1_title');
            $table->dropColumn('hero_button_title');
            $table->dropColumn('our_services_title');
            $table->dropColumn('our_services');
            $table->dropColumn('our_services_btn_title');
            $table->dropColumn('about_as_title');
            $table->dropColumn('about_us_paragraphs');
            $table->dropColumn('about_us_you_can_save_prefix');
            $table->dropColumn('about_us_you_can_save_percent');
            $table->dropColumn('about_us_you_can_save_text');
            $table->dropColumn('about_as_img');
            $table->dropColumn('about_as_img_title');
            $table->dropColumn('about_as_img_alt');
            $table->dropColumn('white_logo');
            $table->dropColumn('running_text');
            $table->dropColumn('outstaffing_benefits_title');
            $table->dropColumn('outstaffing_benefits_items');
            $table->dropColumn('hire_a_team_title');
            $table->dropColumn('hire_a_team_description');
            $table->dropColumn('hire_a_team_btn_title');
            $table->dropColumn('partners');
            $table->dropColumn('form_title');
            $table->dropColumn('form_description');
        });
    }
}
