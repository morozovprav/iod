<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEnglishLvlCheckedToSpecialistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('specialists', function (Blueprint $table) {
            $table->integer('english_is_checked')->after('english_level_id');
        });
        Schema::table('english_levels', function (Blueprint $table) {
            $table->dropColumn('checked');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('specialists', function (Blueprint $table) {
            $table->dropColumn('english_is_checked');
        });
        Schema::table('english_levels', function (Blueprint $table) {
            $table->integer('checked')->nullable();
        });
    }
}
