<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecialistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specialists', function (Blueprint $table) {
            $table->id();
            $table->json('meta_title')->nullable();
            $table->json('meta_description')->nullable();
            $table->json('meta_keyword')->nullable();
            $table->json('og_title')->nullable();
            $table->json('og_description')->nullable();
            $table->string('og_img')->nullable();
            $table->json('first_name');
            $table->string('main_img')->nullable();
            $table->json('last_name');
            $table->json('about_me')->nullable();
            $table->json('projects')->nullable();
            $table->json('certificates')->nullable();
            $table->json('courses')->nullable();
            $table->bigInteger('english_level_id')->unsigned()->nullable();
            $table->foreign('english_level_id')->references('id')->on('english_levels')->onDelete('cascade');
            $table->bigInteger('profession_id')->unsigned()->nullable();
            $table->foreign('profession_id')->references('id')->on('professions')->onDelete('cascade');
            $table->bigInteger('status_id')->unsigned()->nullable();
            $table->foreign('status_id')->references('id')->on('statuses')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specialists');
    }
}
