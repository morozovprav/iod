<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFormFieldToSpecialistPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('specialist_pages', function (Blueprint $table) {
            $table->json('popup_title')->nullable();
            $table->json('popup_description')->nullable();
            $table->json('popup_btn_title')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('specialist_pages', function (Blueprint $table) {
            $table->dropColumn('form_title');
            $table->dropColumn('form_description');
            $table->dropColumn('form_btn_title');
        });
    }
}
