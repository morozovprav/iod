<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFormFieldToTeamsPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teams_pages', function (Blueprint $table) {
            $table->json('popup_title')->nullable();
            $table->json('popup_description')->nullable();
            $table->json('popup_btn_title')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teams_pages', function (Blueprint $table) {
            $table->dropColumn('popup_title');
            $table->dropColumn('popup_description');
            $table->dropColumn('popup_btn_title');
        });
    }
}
