<?php

use App\Http\Controllers\About;
use App\Http\Controllers\Contact;
use App\Http\Controllers\Filters;
use App\Http\Controllers\FilterSearch;
use App\Http\Controllers\Page404;
use App\Http\Controllers\Parts\Parts;
use App\Http\Controllers\Popup;
use App\Http\Controllers\Privacy;
use App\Http\Controllers\SeoController;
use App\Http\Controllers\SpecialistsPage;
use App\Http\Controllers\Teams;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SpecialistPage;
use App\Http\Controllers\Specialist;
use App\Http\Controllers\Main;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//popups
Route::post('/popups_send',[Popup::class,'requestPopupsPost']);

//pages
Route::get('/main',[Main::class,'index']);
Route::get('/contact', [Contact::class, 'index']);
Route::get('/about',[About::class, 'index']);
Route::get('/specialist_page',[SpecialistPage::class,'index']);
Route::get('/specialists_page', [SpecialistsPage::class, 'index']);
Route::get('/404', [Page404::class, 'index']);
Route::get('/privacy', [Privacy::class, 'index']);

//Teams
Route::get('/teams', [Teams::class, 'getTeamsPage']);
Route::get('/team/{slug}', [Teams::class, 'getOneTeam']);

//Parts
Route::get('/parts', [Parts::class, 'index']);

//members
Route::get('/specialist/{slug}',[Specialist::class,'getOneSpecialist']);

//Filters
Route::get('/specialists',[Specialist::class,'getSpecialists'])->name('specialists');
Route::get('/specialists/{slug}',[SeoController::class,'getMetaIntoSlug']);
Route::get('/filters',[Filters::class,'getFilters']);

//FilterSearch
Route::get('/filter-search',[FilterSearch::class,'filterSearch']);

//Seo
Route::controller(SeoController::class)->group(function (){
    Route::get('/slugs', 'getSlugs');
    Route::get('/filter-slugs', 'getFilterSlugs');
    Route::get('/specific-filter-slugs', 'getSlugsForSeo');
    Route::get('/generate', 'generate');
});
//Route::get('/slugs', [SeoController::class, 'getSlugs']);
//Route::get('/filter-slugs', [SeoController::class, 'getFilterSlugs']);
//Route::get('/specific-filter-slugs', [SeoController::class, 'getSlugsForSeo']);
//Route::get('/generate', [SeoController::class, 'generate']);


